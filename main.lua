require "System"
require "FontManager"
require "Game"

---------------------------------------------------------------------------------------------------

KEYS = {} -- This array is filled in by HandleEvent and can be read at any time to check if a key is pressed

-- Return true from this function when game should stop
function HandleEvent(event)
	local c = event.type
	if c == SDL.event.KeyDown then
		local key = event.keysym.sym
		if key == SDL.key.Escape then
			return true
		else
			KEYS[key] = true
		end
	elseif c == SDL.event.KeyUp then
		local key = event.keysym.sym
		KEYS[key] = false
	elseif c == SDL.event.Quit then
		return true
	end

	return false
end

---------------------------------------------------------------------------------------------------

function main()
	-- Initialize SDL
	SystemInit(LEVEL_WIDTH * TILE_SIZE, (LEVEL_HEIGHT + 1) * TILE_SIZE)

	-- Initialize the game (GAME will refer to the instance of Game)
	Game:new()

	-- Main loop
	local delta_time = 0
	local stop_game
	repeat
		-- Fill screen, otherwise SDL alpha blending doesn't work very well. Pure black.
		SYS_RENDERER:setDrawColor(0x000000)
		SYS_RENDERER:clear()

		GAME:update(delta_time)
		GAME:draw()

		--
		stop_game, delta_time = SystemUpdate()
	until stop_game == true

	--
	SystemQuit()
end

main()
