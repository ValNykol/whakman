require "System"
require "FontManager"
require "Levels"

---------------------------------------------------------------------------------------------------

KEYS = {} -- This array is filled in by HandleEvent and can be read at any time to check if a key is pressed
MOUSE = { x = 0, y = 0, left = false, right = false }

CURRENT_LEVEL = nil

AUTOSAVE = true

ROOT_MENU = nil

-- Return true from this function when game should stop
function HandleEvent(event)
	local c = event.type
	if c == SDL.event.KeyDown then
		local key = event.keysym.sym
		KEYS[key] = true
	elseif c == SDL.event.KeyUp then
		local key = event.keysym.sym
		KEYS[key] = false
	elseif c == SDL.event.MouseButtonDown then
		local button = event.button
		if		button == SDL.mouseButton.Left then
			MOUSE.left = true
		elseif	button == SDL.mouseButton.Right then
			MOUSE.right = true
		end
	elseif c == SDL.event.MouseButtonUp then
		local button = event.button
		if		button == SDL.mouseButton.Left then
			MOUSE.left = false
		elseif	button == SDL.mouseButton.Right then
			MOUSE.right = false
		end
	elseif c == SDL.event.MouseMotion then
		MOUSE.x = event.x
		MOUSE.y = event.y
	elseif c == SDL.event.Quit then
		if AUTOSAVE then
			GenerateLevelMap()
			WriteLevelsFile()
		end
		return true
	end

	return false
end

---------------------------------------------------------------------------------------------------

-- Class for something drawn on the screen

Actor = class()
Actor.hover_phase = 0

-- "Static" method: to make sure all the hovering objects are in sync (even if created later)
function Actor.update_hover(dt)
	Actor.hover_phase = Actor.hover_phase + 8 * dt
	if Actor.hover_phase > 2 * math.pi then
		Actor.hover_phase = Actor.hover_phase - 2 * math.pi
	end
end

function Actor:init(image, centered, hover)
	self.image = image

	-- Position on screen
	self.x = 0
	self.y = 0

	self.abs_x = 0
	self.abs_y = 0

	-- Whether position refers to the top left corner or the center
	self.centered = centered

	-- Hover effect for collectible objects
	self.hover = hover

	if self.hover then
		self.hover_amp = 2
	end

	self.visible = true
end

function Actor:draw(parent_x, parent_y)
	if self.visible then
		-- Get position on screen
		local x, y
		if self.centered then
			x = math.floor(self.x - self.image.w / 2)
			y = math.floor(self.y - self.image.h / 2)
		else
			x = self.x
			y = self.y
		end

		if self.parent then
			x = x + parent_x
			y = y + parent_y
		end

		if self.hover then
			y = y + math.floor(math.sin(Actor.hover_phase) * self.hover_amp + 0.5)
		end

		self.abs_x = x
		self.abs_y = y

		-- Render
		RenderImage(self.image, x, y)
	end
end

---------------------------------------------------------------------------------------------------

-- Directions

DIRECTIONS = {
	up = 1,
	right = 2,
	down = 3,
	left = 4
}

---------------------------------------------------------------------------------------------------

-- Tile images

-- This array stores all the allowed tiles and the corresponding images
TILE_IMAGES = {
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_cross.png" } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_straight.png" } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_straight.png",	rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_solid.png" } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_t.png" } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_t.png",		rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_t.png",		rotation = 180 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_t.png",		rotation = 270 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_turn.png" } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_turn.png",		rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_turn.png",		rotation = 180 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_turn.png",		rotation = 270 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_end.png" } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_end.png",		rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_end.png",		rotation = 180 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_end.png",		rotation = 270 } }
}

-- Should be called at the start (after SDL is initialized) to cache all the tile images
function InitTileImages()
	for _, tile_image in ipairs(TILE_IMAGES) do
		tile_image.image = Image(tile_image.image_params.file, tile_image.image_params.rotation, tile_image.image_params.flip)
	end
end

-- available_directions: an array with directions as indices and values true or false (indicating the allowed movement from this tile)
-- Returns the corresponding image
function GetTileImageAndType(available_directions)
	for tile_type, tile_image in ipairs(TILE_IMAGES) do
		local is_match = true
		for dir, available in pairs(available_directions) do
			if available ~= tile_image.available_directions[dir] then
				is_match = false
				break
			end
		end

		if is_match then
			return tile_image.image, tile_type
		end
	end
end

---------------------------------------------------------------------------------------------------

CONTENT_IMAGES = {
	[TILE_CONTENT.whakman]	= { image_params = { file = "whakman_02.png" } },
	[TILE_CONTENT.hackman]	= { image_params = { file = "hackman_02.png" } },
	[TILE_CONTENT.coin]		= { image_params = { file = "coin_small_editor.png" } },
	[TILE_CONTENT.rock]		= { image_params = { file = "rock.png" } }
}

function InitContentImages()
	for _, content_image in pairs(CONTENT_IMAGES) do
		content_image.image = Image(content_image.image_params.file, content_image.image_params.rotation, content_image.image_params.flip)
	end
end

function CreateContentActor(content_type)
	local content_actor
	if		content_type == TILE_CONTENT.whakman then
		content_actor = Actor:new(CONTENT_IMAGES[content_type].image, true)
	elseif	content_type == TILE_CONTENT.hackman then
		content_actor = Actor:new(CONTENT_IMAGES[content_type].image, true)
	elseif	content_type == TILE_CONTENT.coin then
		content_actor = Actor:new(CONTENT_IMAGES[content_type].image, true, true)
	elseif	content_type == TILE_CONTENT.rock then
		content_actor = Actor:new(CONTENT_IMAGES[content_type].image, true, true)
	end
	return content_actor
end

---------------------------------------------------------------------------------------------------

-- Tile

Tile = class(Actor)

function Tile:init(available_directions)
	self.available_directions = available_directions
	local image, tile_type = GetTileImageAndType(self.available_directions)
	self.tile_type = tile_type
	Actor.init(self, image)
end

function Tile:update(dt)
	if MOUSE.x >= self.abs_x and MOUSE.x <= self.abs_x + self.image.w
	and MOUSE.y >= self.abs_y and MOUSE.y <= self.abs_y + self.image.h then
		if MOUSE.left and not MOUSE.right then
			self:on_drag_left()
		elseif MOUSE.right and not MOUSE.left then
			self:on_drag_right()
		end
	end
end

function Tile:on_drag_left()
	if self.parent:get_element("entrance_marker").is_dragging
	or self.parent:get_element("exit_marker").is_dragging then
		return
	end

	local selected_item = ROOT_MENU:get_element("tile_selection"):get_element("selected_item")
	if selected_item.selected_type == ELEMENT_TYPE.tile_option then
		self:change_tile_type(selected_item.type)
	elseif selected_item.selected_type == ELEMENT_TYPE.content_option then
		if not self.content and not self:is_solid_wall() then
			self:add_content(selected_item.type)
		end
	end
end

function Tile:on_drag_right()
	self:remove_content()
end

function Tile:change_tile_type(tile_type)
	if tile_type ~= self.tile_type then
		for direction, available in pairs(TILE_IMAGES[tile_type].available_directions) do
			if self.available_directions[direction] ~= available then
				self:adjust_direction(direction, available)

				if		direction == DIRECTIONS.up and self.tile_y > 1 then
					TILES[self.tile_y - 1][self.tile_x]:adjust_direction(DIRECTIONS.down, available, true)
				elseif	direction == DIRECTIONS.right and self.tile_x < LEVEL_WIDTH then
					TILES[self.tile_y][self.tile_x + 1]:adjust_direction(DIRECTIONS.left, available, true)
				elseif	direction == DIRECTIONS.down and self.tile_y < LEVEL_HEIGHT then
					TILES[self.tile_y + 1][self.tile_x]:adjust_direction(DIRECTIONS.up, available, true)
				elseif	direction == DIRECTIONS.left and self.tile_x > 1 then
					TILES[self.tile_y][self.tile_x - 1]:adjust_direction(DIRECTIONS.right, available, true)
				end
			end
		end
		self:refresh_type_and_image()

		if self:is_solid_wall() then
			self:remove_content()
		end
	end
end

function Tile:add_content(content_type)
	if content_type == TILE_CONTENT.whakman or content_type == TILE_CONTENT.hackman then
		RemoveContentOfType(content_type)
	end

	self.content = CreateContentActor(content_type)
	self.content_type = content_type
	self.content.parent = self
	self.content.x = TILE_SIZE / 2
	self.content.y = TILE_SIZE / 2
end

function Tile:remove_content()
	if self.content then
		self.content = nil
		self.content_type = nil
	end
end

function Tile:is_solid_wall()
	for direction, available in pairs(self.available_directions) do
		if available then
			return false
		end
	end

	return true
end

function Tile:adjust_direction(direction, available, refresh)
	if self.available_directions[direction] ~= available then
		self.available_directions[direction] = available

		if (direction == DIRECTIONS.up and self.tile_y == 1)
		or (direction == DIRECTIONS.down and self.tile_y == LEVEL_HEIGHT) then
			self.available_directions[direction] = false
		end
		if (direction == DIRECTIONS.right and self.tile_x == LEVEL_WIDTH)
		or (direction == DIRECTIONS.left and self.tile_x == 1) then
			self.available_directions[direction] = (self.tile_y == LEVELS[CURRENT_LEVEL].exit_y)
		end

		if refresh then
			self:refresh_type_and_image()
		end
	end
end

function Tile:refresh_type_and_image()
	self.image, self.tile_type = GetTileImageAndType(self.available_directions)

	if self:is_solid_wall() then
		self:remove_content()
	end
end

function Tile:draw(parent_x, parent_y)
	Actor.draw(self, parent_x, parent_y)
	if self.content then
		self.content:draw(self.x + parent_x, self.y + parent_y)
	end
end

---------------------------------------------------------------------------------------------------

TILES = {}
TILE_SIZE = 64

TILE_SELECTION_WIDTH = 8
TILE_SELECTION_HEIGHT = 13

function SetupTiles(level_editing_menu)
	for y = 1, LEVEL_HEIGHT do
		TILES[y] = {}

		for x = 1, LEVEL_WIDTH do
			local available_directions = {}
			for _, direction in pairs(DIRECTIONS) do
				available_directions[direction] = false
			end

			local tile = Tile:new(available_directions)

			-- Pixel position
			tile.x = x * TILE_SIZE
			tile.y = (y - 1) * TILE_SIZE

			-- Tile position
			tile.tile_x = x
			tile.tile_y = y
			level_editing_menu:add_child(tile)

			TILES[y][x] = tile
		end
	end
end

function LoadLevel(level)
	if CURRENT_LEVEL then
		GenerateLevelMap()
	end

	CURRENT_LEVEL = level

	for y = 1, LEVEL_HEIGHT do
		for x = 1, LEVEL_WIDTH do
			-- Each tile is represented by 3x3 square of characters in LEVELS
			-- level_x and level_y will point to the center of that square
			local level_x, level_y = (x - 1) * 3 + 2, (y - 1) * 3 + 2

			local tile = TILES[y][x]
			tile:remove_content()

			tile:adjust_direction(DIRECTIONS.up,	LEVELS[CURRENT_LEVEL].map[level_y - 1]:sub(level_x, level_x) == TILE_CONTENT.empty)
			tile:adjust_direction(DIRECTIONS.right,	LEVELS[CURRENT_LEVEL].map[level_y]:sub(level_x + 1, level_x + 1) == TILE_CONTENT.empty)
			tile:adjust_direction(DIRECTIONS.down,	LEVELS[CURRENT_LEVEL].map[level_y + 1]:sub(level_x, level_x) == TILE_CONTENT.empty)
			tile:adjust_direction(DIRECTIONS.left,	LEVELS[CURRENT_LEVEL].map[level_y]:sub(level_x - 1, level_x - 1) == TILE_CONTENT.empty)

			tile:refresh_type_and_image()

			local content_type = LEVELS[CURRENT_LEVEL].map[level_y]:sub(level_x, level_x)
			if content_type ~= TILE_CONTENT.wall and content_type ~= TILE_CONTENT.empty then
				tile:add_content(content_type)
			end
		end
	end

	AdjustExitMarkers()
	RefreshLevelLabel()
end

function RemoveContentOfType(content_type)
	for y = 1, LEVEL_HEIGHT do
		for x = 1, LEVEL_WIDTH do
			if TILES[y] and TILES[y][x] and TILES[y][x].content_type == content_type then
				TILES[y][x]:remove_content()
			end
		end
	end
end

function GenerateLevelMap()
	LEVELS[CURRENT_LEVEL].map = {}
	for y = 1, LEVEL_HEIGHT do
		LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 1] = ""
		LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 2] = ""
		LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 3] = ""
		for x = 1, LEVEL_WIDTH do
			local tile = TILES[y][x]

			LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 1] = LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 1] .. (tile.available_directions[DIRECTIONS.up] and "#.#" or "###")

			LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 2] = LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 2] .. (tile.available_directions[DIRECTIONS.left] and "." or "#")
			LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 2] = LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 2] .. (tile:is_solid_wall() and "#" or (tile.content_type or "."))
			LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 2] = LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 2] .. (tile.available_directions[DIRECTIONS.right] and "." or "#")

			LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 3] = LEVELS[CURRENT_LEVEL].map[(y - 1) * 3 + 3] .. (tile.available_directions[DIRECTIONS.down] and "#.#" or "###")
		end
	end
end

---------------------------------------------------------------------------------------------------

-- Menus

Menu = class()

function Menu:init(children)
	self.children = {}
	self.elements = {}

	if children then
		for _, child_layout in ipairs(children) do
			local child

			if		child_layout.element_type == ELEMENT_TYPE.tile_option then
				child = TileOption:new(child_layout.tile_type)
			elseif	child_layout.element_type == ELEMENT_TYPE.content_option then
				child = ContentOption:new(child_layout.content_type)
			elseif	child_layout.element_type == ELEMENT_TYPE.selected_item then
				child = SelectedItem:new()
			elseif	child_layout.element_type == ELEMENT_TYPE.button then
				child = Button:new(Image(child_layout.image, child_layout.rotation, child_layout.flip))
				child.on_click = child_layout.on_click
			elseif	child_layout.element_type == ELEMENT_TYPE.label then
				local color
				if child_layout.color then
					color = child_layout.color
				else
					color = { r = 0xff, g = 0xff, b = 0xff, a = 0xff }
				end

				local font = LoadFont(child_layout.font, child_layout.size)

				local horz_alignment = child_layout.horz_alignment or HORZ_ALIGNMENT.left
				local vert_alignment = child_layout.vert_alignment or VERT_ALIGNMENT.top

				child = Label:new(font, color, child_layout.text, horz_alignment, vert_alignment)
			elseif	child_layout.element_type == ELEMENT_TYPE.exit_marker then
				child = ExitMarker:new(Image(child_layout.image, child_layout.rotation, child_layout.flip))
			elseif	child_layout.element_type == ELEMENT_TYPE.menu then
				child = Menu:new(child_layout.children)
				child.w = child_layout.w
				child.h = child_layout.h
			end

			if child then
				child.x = child_layout.x
				child.y = child_layout.y
				child.parent = self

				table.insert(self.children, child)

				if child_layout.id then
					self.elements[child_layout.id] = child
				end
			end
		end
	end

	self.x = 0
	self.y = 0

	self.mouse_past = { left = false, right = false }
end

function Menu:add_child(child, index)
	child.parent = self

	if index then
		table.insert(self.children, index, child)
	else
		table.insert(self.children, child)
	end
end

function Menu:get_element(id)
	return self.elements[id]
end

function Menu:update(dt)
	if MOUSE.left ~= self.mouse_past.left then
		for _, child in ipairs(self.children) do
			if child.image then
				if MOUSE.x >= child.abs_x and MOUSE.x <= child.abs_x + child.image.w
				and MOUSE.y >= child.abs_y and MOUSE.y <= child.abs_y + child.image.h then
					if MOUSE.left then
						if child.on_press and child.visible then
							child:on_press()
						end
					else
						if child.on_release and child.visible then
							child:on_release()
						end
					end
				end
			end
		end
	end

	for _, child in ipairs(self.children) do
		if child.update then
			child:update(dt)
		end
	end

	self.mouse_past.left, self.mouse_past.right = MOUSE.left, MOUSE.right
end

function Menu:draw()
	for _, child in ipairs(self.children) do
		child:draw(self.x, self.y)
	end
end

ELEMENT_TYPE = {
	tile_option = 1,
	content_option = 2,
	selected_item = 3,
	button = 4,
	label = 5,
	exit_marker = 6,
	menu = 7
}

MENU_LAYOUT = {
	{
		element_type = ELEMENT_TYPE.menu,
		id = "level_editing",
		w = LEVEL_WIDTH * TILE_SIZE + 64 * 2,
		h = LEVEL_HEIGHT * TILE_SIZE,
		children = {
			{ element_type = ELEMENT_TYPE.exit_marker, id = "entrance_marker", image = "red_arrow.png", x = 0, y = 0 },
			{ element_type = ELEMENT_TYPE.exit_marker, id = "exit_marker", image = "red_arrow.png", x = (LEVEL_WIDTH + 1) * TILE_SIZE, y = 0 }
		}
	},
	{
		element_type = ELEMENT_TYPE.menu,
		id = "tile_selection",
		w = TILE_SELECTION_WIDTH * TILE_SIZE,
		h = TILE_SELECTION_HEIGHT * TILE_SIZE,
		children = {
			{
				element_type = ELEMENT_TYPE.label,
				font = "LondonBetween.ttf",
				size = 32,
				text = "Selected:",
				horz_alignment = HORZ_ALIGNMENT.center,
				vert_alignment = VERT_ALIGNMENT.center,
				color = {r = 0xff, g = 0xff, b = 0xff, a = 0xff},
				x = 3.5 * TILE_SIZE,
				y = 1.5 * TILE_SIZE
			},

			{ element_type = ELEMENT_TYPE.selected_item, id = "selected_item", x = 5 * TILE_SIZE, y = 1 * TILE_SIZE },

			{
				element_type = ELEMENT_TYPE.label,
				font = "LondonBetween.ttf",
				size = 32,
				text = "Tile options:",
				horz_alignment = HORZ_ALIGNMENT.center,
				vert_alignment = VERT_ALIGNMENT.center,
				color = {r = 0xff, g = 0xff, b = 0xff, a = 0xff},
				x = 4 * TILE_SIZE,
				y = 2.5 * TILE_SIZE
			},

			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 1,	x = 2 * TILE_SIZE,		y = 3 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 2,	x = 2 + 3 * TILE_SIZE,	y = 3 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 3,	x = 4 + 4 * TILE_SIZE,	y = 3 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 4,	x = 6 + 5 * TILE_SIZE,	y = 3 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 5,	x = 2 * TILE_SIZE,		y = 2 + 4 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 6,	x = 2 + 3 * TILE_SIZE,	y = 2 + 4 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 7,	x = 4 + 4 * TILE_SIZE,	y = 2 + 4 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 8,	x = 6 + 5 * TILE_SIZE,	y = 2 + 4 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 9,	x = 2 * TILE_SIZE,		y = 4 + 5 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 10,	x = 2 + 3 * TILE_SIZE,	y = 4 + 5 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 11,	x = 4 + 4 * TILE_SIZE,	y = 4 + 5 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 12,	x = 6 + 5 * TILE_SIZE,	y = 4 + 5 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 13,	x = 2 * TILE_SIZE,		y = 6 + 6 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 14,	x = 2 + 3 * TILE_SIZE,	y = 6 + 6 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 15,	x = 4 + 4 * TILE_SIZE,	y = 6 + 6 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.tile_option, tile_type = 16,	x = 6 + 5 * TILE_SIZE,	y = 6 + 6 * TILE_SIZE },

			{
				element_type = ELEMENT_TYPE.label,
				font = "LondonBetween.ttf",
				size = 32,
				text = "Content options:",
				horz_alignment = HORZ_ALIGNMENT.center,
				vert_alignment = VERT_ALIGNMENT.center,
				color = {r = 0xff, g = 0xff, b = 0xff, a = 0xff},
				x = 4 * TILE_SIZE,
				y = 7.5 * TILE_SIZE
			},

			{ element_type = ELEMENT_TYPE.content_option, content_type = TILE_CONTENT.whakman,	x = 2 * TILE_SIZE,	y = 8 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.content_option, content_type = TILE_CONTENT.hackman,	x = 3 * TILE_SIZE,	y = 8 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.content_option, content_type = TILE_CONTENT.coin,		x = 4 * TILE_SIZE,	y = 8 * TILE_SIZE },
			{ element_type = ELEMENT_TYPE.content_option, content_type = TILE_CONTENT.rock,		x = 5 * TILE_SIZE,	y = 8 * TILE_SIZE },

			{ element_type = ELEMENT_TYPE.button, id = "remove_button", image = "cross.png", x = 2 * TILE_SIZE, y = 10 * TILE_SIZE, on_click = function(self)
				if CURRENT_LEVEL == #LEVELS then
					LoadLevel(CURRENT_LEVEL - 1) -- will decrease CURRENT_LEVEL
					table.remove(LEVELS)
				else
					LoadLevel(CURRENT_LEVEL + 1) -- will increase CURRENT_LEVEL
					table.remove(LEVELS, CURRENT_LEVEL - 1)
					CURRENT_LEVEL = CURRENT_LEVEL - 1
				end

				RefreshLevelLabel()

				if CURRENT_LEVEL == 1 then
					self.parent:get_element("left_button").visible = false
				end
				if CURRENT_LEVEL == #LEVELS then
					self.parent:get_element("right_button").visible = false
					self.parent:get_element("add_button").visible = true
				end

				if #LEVELS == 1 then
					self.visible = false
				end
			end },

			{ element_type = ELEMENT_TYPE.button, id = "left_button", image = "menu_arrow.png", x = 1 * TILE_SIZE, y = 11 * TILE_SIZE, on_click = function(self)
				LoadLevel(CURRENT_LEVEL - 1)
				if CURRENT_LEVEL == 1 then
					self.visible = false
				end
				self.parent:get_element("right_button").visible = true
				self.parent:get_element("add_button").visible = false
			end },

			{
				element_type = ELEMENT_TYPE.label,
				id = "level_label",
				font = "LondonBetween.ttf",
				size = 32,
				text = "",
				horz_alignment = HORZ_ALIGNMENT.center,
				vert_alignment = VERT_ALIGNMENT.center,
				color = {r = 0xff, g = 0xff, b = 0xff, a = 0xff},
				x = 2.5 * TILE_SIZE,
				y = 11.5 * TILE_SIZE
			},

			{ element_type = ELEMENT_TYPE.button, id = "right_button", image = "menu_arrow.png", rotation = 180, x = 3 * TILE_SIZE, y = 11 * TILE_SIZE, on_click = function(self)
				LoadLevel(CURRENT_LEVEL + 1)
				if CURRENT_LEVEL == #LEVELS then
					self.visible = false
					self.parent:get_element("add_button").visible = true
				end
				self.parent:get_element("left_button").visible = true
			end },

			{ element_type = ELEMENT_TYPE.button, id = "add_button", image = "plus.png", x = 3 * TILE_SIZE, y = 11 * TILE_SIZE, on_click = function(self)
				table.insert(LEVELS, {})
				local new_level = LEVELS[#LEVELS]
				new_level.map = {}
				for y = 1, LEVEL_HEIGHT do
					new_level.map[(y - 1) * 3 + 1] = ""
					new_level.map[(y - 1) * 3 + 2] = ""
					new_level.map[(y - 1) * 3 + 3] = ""
					for x = 1, LEVEL_WIDTH do
						if y == 1 then
							new_level.map[(y - 1) * 3 + 1] = new_level.map[(y - 1) * 3 + 1] .. "###"
						else
							new_level.map[(y - 1) * 3 + 1] = new_level.map[(y - 1) * 3 + 1] .. "#.#"
						end

						if x > 1 or y == 1 then
							new_level.map[(y - 1) * 3 + 2] = new_level.map[(y - 1) * 3 + 2] .. "."
						else
							new_level.map[(y - 1) * 3 + 2] = new_level.map[(y - 1) * 3 + 2] .. "#"
						end

						new_level.map[(y - 1) * 3 + 2] = new_level.map[(y - 1) * 3 + 2] .. "."

						if x < LEVEL_WIDTH or y == 1 then
							new_level.map[(y - 1) * 3 + 2] = new_level.map[(y - 1) * 3 + 2] .. "."
						else
							new_level.map[(y - 1) * 3 + 2] = new_level.map[(y - 1) * 3 + 2] .. "#"
						end

						if y == LEVEL_HEIGHT then
							new_level.map[(y - 1) * 3 + 3] = new_level.map[(y - 1) * 3 + 3] .. "###"
						else
							new_level.map[(y - 1) * 3 + 3] = new_level.map[(y - 1) * 3 + 3] .. "#.#"
						end
					end
				end
				new_level.exit_y = 1

				LoadLevel(#LEVELS)

				self.parent:get_element("left_button").visible = true
				self.parent:get_element("remove_button").visible = true
			end },

			{ element_type = ELEMENT_TYPE.button, image = "checkbox_filled.png", x = 4 * TILE_SIZE, y = 10 * TILE_SIZE, on_click = function(self)
				AUTOSAVE = not AUTOSAVE
				if AUTOSAVE then
					self.image = Image("checkbox_filled.png")
				else
					self.image = Image("checkbox_empty.png")
				end
			end },

			{
				element_type = ELEMENT_TYPE.label,
				font = "LondonBetween.ttf",
				size = 32,
				text = "Autosave",
				horz_alignment = HORZ_ALIGNMENT.left,
				vert_alignment = VERT_ALIGNMENT.center,
				color = {r = 0xff, g = 0xff, b = 0xff, a = 0xff},
				x = 5 * TILE_SIZE,
				y = 10.5 * TILE_SIZE
			},

			{ element_type = ELEMENT_TYPE.button, image = "blue_button.png", x = 5 * TILE_SIZE, y = 11 * TILE_SIZE, on_click = function(self)
				GenerateLevelMap()
				WriteLevelsFile()
			end },

			{
				element_type = ELEMENT_TYPE.label,
				font = "LondonBetween.ttf",
				size = 32,
				text = "Save",
				horz_alignment = HORZ_ALIGNMENT.center,
				vert_alignment = VERT_ALIGNMENT.center,
				color = {r = 0xff, g = 0xff, b = 0xff, a = 0xff},
				x = 6 * TILE_SIZE,
				y = 11.5 * TILE_SIZE
			}
		}
	}
}

---------------------------------------------------------------------------------------------------

-- Menu elements

TileOption = class(Actor)

function TileOption:init(tile_type)
	self.tile_type = tile_type
	Actor.init(self, TILE_IMAGES[tile_type].image)
end

function TileOption:on_press()
	self.parent:get_element("selected_item"):select_tile(self.tile_type)
end


ContentOption = class(Actor)

function ContentOption:init(content_type)
	self.content_type = content_type
	Actor.init(self, CONTENT_IMAGES[content_type].image)
end

function ContentOption:on_press()
	self.parent:get_element("selected_item"):select_content(self.content_type)
end


SelectedItem = class(Actor)

function SelectedItem:init()
	self.selected_type = ELEMENT_TYPE.tile_option
	self.type = 1
	Actor.init(self, TILE_IMAGES[self.type].image)

	self.frame = Actor:new(Image("grey_frame.png"))
	self.frame.parent = self
end

function SelectedItem:select_tile(tile_type)
	self.selected_type = ELEMENT_TYPE.tile_option
	self.type = tile_type
	self.image = TILE_IMAGES[self.type].image
end

function SelectedItem:select_content(content_type)
	self.selected_type = ELEMENT_TYPE.content_option
	self.type = content_type
	self.image = CONTENT_IMAGES[self.type].image
end

function SelectedItem:draw(parent_x, parent_y)
	Actor.draw(self, parent_x, parent_y)
	self.frame:draw(self.x + parent_x, self.y + parent_y)
end


Button = class(Actor)

function Button:init(image)
	Actor.init(self, image)
	self.was_pressed = false
end

function Button:on_press()
	self.was_pressed = true
end

function Button:on_release()
	if self.was_pressed then
		self:on_click()
		self.was_pressed = false
	end
end

function Button:update(dt)
	if MOUSE.x < self.abs_x or MOUSE.x > self.abs_x + self.image.w
	or MOUSE.y < self.abs_y or MOUSE.y > self.abs_y + self.image.h then
		self.was_pressed = false
	end
end


Label = class()

function Label:init(font, color, text, horz_alignment, vert_alignment)
	self.font = font
	self.color = color
	self.text = text
	self.horz_alignment = horz_alignment
	self.vert_alignment = vert_alignment

	self.x = 0
	self.y = 0
end

function Label:draw(parent_x, parent_y)
	local x = self.x
	local y = self.y

	if self.parent then
		x = x + parent_x
		y = y + parent_y
	end

	self.font:print_aligned(x, y, self.color, self.horz_alignment, self.vert_alignment, self.text)
end


ExitMarker = class(Actor)

function ExitMarker:init(image)
	Actor.init(self, image)
	self.is_dragging = false
end

function ExitMarker:on_press()
	self.is_dragging = true
end

function ExitMarker:update(dt)
	if not MOUSE.left then
		self.is_dragging = false
	end

	if self.is_dragging then
		local new_y = math.floor((MOUSE.y - self.parent.y) / TILE_SIZE) + 1
		if new_y >= 1 and new_y <= LEVEL_HEIGHT and new_y ~= LEVELS[CURRENT_LEVEL].exit_y then
			local old_y = LEVELS[CURRENT_LEVEL].exit_y
			LEVELS[CURRENT_LEVEL].exit_y = new_y

			TILES[old_y][1]:adjust_direction(DIRECTIONS.left, false, true)
			TILES[new_y][1]:adjust_direction(DIRECTIONS.left, true, true)
			TILES[old_y][LEVEL_WIDTH]:adjust_direction(DIRECTIONS.right, false, true)
			TILES[new_y][LEVEL_WIDTH]:adjust_direction(DIRECTIONS.right, true, true)

			AdjustExitMarkers()
		end
	end
end

---------------------------------------------------------------------------------------------------

function RefreshLevelLabel()
	ROOT_MENU:get_element("tile_selection"):get_element("level_label").text = CURRENT_LEVEL .. "/" .. #LEVELS
end

function AdjustExitMarkers()
	ROOT_MENU:get_element("level_editing"):get_element("entrance_marker").y	= (LEVELS[CURRENT_LEVEL].exit_y - 1) * TILE_SIZE
	ROOT_MENU:get_element("level_editing"):get_element("exit_marker").y		= (LEVELS[CURRENT_LEVEL].exit_y - 1) * TILE_SIZE
end

---------------------------------------------------------------------------------------------------

LEVELS_BEFORE_INFO = nil
LEVELS_AFTER_INFO = nil
LEVELS_ADDITIONAL_INFO = {}

function ReadLevelsFile()
	local file = io.open("lua\\Levels.lua", "r")
	local content = file:read("*all")
	file:close()

	file = io.open("lua\\Levels_backup.lua", "w")
	file:write(content)
	file:close()

	local e
	_, e, LEVELS_BEFORE_INFO = content:find("^(.-)LEVELS = ")
	content = content:sub(e + 1)
	_, _, LEVELS_AFTER_INFO = content:find("^%b{}(.*)$")

	for index, additional_info in content:gmatch("%[([0-9]+)%] = {%s+%-%- level_start%s+map = %b{},\n\t\texit_y = [0-9]+(.-)\n\t\t%-%- level_end") do
		local i = tonumber(index)

		LEVELS_ADDITIONAL_INFO[i] = additional_info
	end
end

function WriteLevelsFile()
	local file = io.open("lua\\Levels.lua", "w")

	file:write(LEVELS_BEFORE_INFO)
	file:write("LEVELS = {\n")

	for i, level in ipairs(LEVELS) do
		file:write("\t["..i.."] = {\n\t\t-- level_start\n\t\tmap = {\n")
		for j, row in ipairs(level.map) do
			file:write("\t\t\t")
			file:write("\""..row.."\"")
			if j < #level.map then
				file:write(",")
			end
			file:write("\n")
		end
		file:write("\t\t},\n")
		file:write("\t\texit_y = "..level.exit_y)
		if LEVELS_ADDITIONAL_INFO[i] then
			file:write(LEVELS_ADDITIONAL_INFO[i])
		end
		file:write("\n\t\t-- level_end\n\t}")
		if i < #LEVELS then
			file:write(",")
		end
		file:write("\n")
	end

	file:write("}")
	file:write(LEVELS_AFTER_INFO)

	file:close()
end

---------------------------------------------------------------------------------------------------

function main()
	-- Initialize SDL
	local screen_width = MENU_LAYOUT[1].w + MENU_LAYOUT[2].w
	local screen_height = math.max(MENU_LAYOUT[1].h, MENU_LAYOUT[2].h)

	SystemInit(screen_width, screen_height)

	ReadLevelsFile()

	-- Initialize tile images
	InitTileImages()
	InitContentImages()

	--
	local font_size = 64
	local font = LoadFont("LondonBetween.ttf", font_size)

	ROOT_MENU = Menu:new(MENU_LAYOUT)
	ROOT_MENU.w, ROOT_MENU.h = screen_width, screen_height

	local level_editing_menu = ROOT_MENU:get_element("level_editing")
	local tile_selection_menu = ROOT_MENU:get_element("tile_selection")

	level_editing_menu.x, level_editing_menu.y = 0, math.floor((screen_height - level_editing_menu.h) / 2)

	SetupTiles(level_editing_menu)

	tile_selection_menu.x, tile_selection_menu.y = level_editing_menu.w, math.floor((screen_height - tile_selection_menu.h) / 2)

	tile_selection_menu:get_element("left_button").visible = false

	if #LEVELS > 1 then
		tile_selection_menu:get_element("add_button").visible = false
	else
		tile_selection_menu:get_element("right_button").visible = false
		tile_selection_menu:get_element("remove_button").visible = false
	end

	-- Add actors to relevant list
	local draw_list = { ROOT_MENU }
	local update_list = { ROOT_MENU }

	LoadLevel(1)

	-- Main loop
	local delta_time = 0
	local stop_game
	repeat
		-- Fill screen, otherwise SDL alpha blending doesn't work very well. Almost black.
		SYS_RENDERER:setDrawColor(0x0f0f0f)
		SYS_RENDERER:clear()

		Actor.update_hover(delta_time)

		-- Update and draw all objects that have been registered
		for _,v in ipairs(update_list)		do v:update(delta_time) end
		for _,v in ipairs(draw_list)		do v:draw() end

		--
		stop_game, delta_time = SystemUpdate()
	until stop_game == true

	--
	SystemQuit()
end

main()
