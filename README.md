# README #

This is a Pacman-inspired game called Whakman vs Hackman. It is written in Lua using SDL2 and luasdl2 (https://github.com/Tangent128/luasdl2).

* Run whakman.bat to play the game
* Run level_editor.bat to edit the levels

### How do I play? ###

* Collect more coins than Hackman
* Don't get eaten by Hackman
* Throw rocks at Hackman with SPACE to stun him
* When all the coins are collected, exit the level to the right

### How do I edit the levels? ###

* Select tiles and tile content on the right side
* Click or drag tiles on the left side to make the level
* The red arrows indicate the entrance and the exit of the level (acts like a tunnel while there are coins)
* Every level should have Whakman, Hackman, some coins and a valid path from entrance to characters' positions and to exit

### Can I use the source code? ###

* Sure, as long as you give me credit for it

*Valentyn Nykoliuk*