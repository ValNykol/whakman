---------------------------------------------------------------------------------------------------

-- OSD items must have at least a value, and can also have a picture and a label

OSDItem = class()

-- Instead of being passed a value every time it is changed, OSD items are provided a getter for the desired value
function OSDItem:init(image, font, color, label, x, y, value_getter)
	self.image = image
	self.font = font
	self.color = color
	self.label = label
	self.value_getter = value_getter
	self.x = x
	self.y = y
end

function OSDItem:draw()
	local spacing = 5

	-- Draw image if we have one. Draw it at x,y
	local x = self.x
	if self.image then
		RenderImage(self.image, x, self.y)
		x = x + self.image.w + spacing
	end

	-- Draw label if we have one. Draw it to the right of what's previously drawn.
	if self.label and self.font then
		local text_image = self.font:print(x, self.y, self.color, self.label)
		x = x + text_image.w + spacing
	end

	-- Draw the value. Draw it to the right of what's previously drawn. An OSD item must have a value.
	self.font:print(x, self.y, self.color, "%i", self.value_getter())
end
