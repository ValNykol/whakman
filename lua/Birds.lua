---------------------------------------------------------------------------------------------------

-- Birds that fly around Hackman's head when he is stunned, inherits from Actor

Birds = class(Actor)

-- Interval for one frame of birds' animation, in seconds
Birds.FRAME_LENGTH = 0.06

-- Offset above Hackman's head (vertical or horizontal depending on Hackman's direction), in pixels
Birds.OFFSET = -10

-- direction: Hackman's current direction
function Birds:init(direction)
	-- Adjust birds' offset and rotation of the images based on Hackman's direction
	local rotation, offset_x, offset_y = 0, 0, 0
	if direction == DIRECTIONS.up or direction == DIRECTIONS.down then
		rotation = 270
		offset_x = Birds.OFFSET
	else
		offset_y = Birds.OFFSET
	end

	-- Images are cycled through to make the animation
	self.images = {	Image("birds_01.png", rotation),
					Image("birds_02.png", rotation),
					Image("birds_03.png", rotation),
					Image("birds_04.png", rotation)}
	self.image_index = 1
	self.animation_timer = 0
	Actor.init(self, self.images[self.image_index], true)
	self.x = offset_x
	self.y = offset_y
end

function Birds:update(dt)
	self.animation_timer = self.animation_timer + dt
	if self.animation_timer >= Birds.FRAME_LENGTH then
		self.animation_timer = self.animation_timer - Birds.FRAME_LENGTH
		self.image_index = self.image_index + 1
		if self.image_index > #self.images then
			-- Set the image_index back to 1 when it exceeds available frames
			self.image_index = 1
		end

		self.image = self.images[self.image_index]
	end
end
