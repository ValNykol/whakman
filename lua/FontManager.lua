--[[-----------------------------------------------------------------------------------------------

Public variables:
	

Public functions:
	___________________

	LoadFont()			Loads a font through SDL_ttf
	- In parameters:	
		file_name		String. Font must reside in Whakman\fonts folder
		size			Integer. The size of the font.
	- Returns			
		Font			Use the returned Font instance to print to screen
	___________________

	Font:print()		Print text with the loaded font and size.
	- In parameters:	
		x,y				Integers. The pixel positions on screen where the text should be rendered
		col				SDL_Color. The color the font should be printed with.
		...				
	- Returns			
	___________________

-----------------------------------------------------------------------------------------------]]--

--
FONTMAN_FONTS = {}

--
function font_table_id(identifier, size)
	return tostring(identifier)..tostring(size)
end

--
function LoadFont(file_name, size)
	local table_id = font_table_id(file_name, size)

	if not FONTMAN_FONTS[table_id] then
		local font, err = SDL_ttf.open("fonts\\"..file_name, size)
		if not font then
			error("Failed to open font "..tostring(file_name)..": "..err.."\n")
		end

		FONTMAN_FONTS[table_id] = Font:new(font)
	end

	return FONTMAN_FONTS[table_id]
end

---------------------------------------------------------------------------------------------------

HORZ_ALIGNMENT = {
	left = 1,
	center = 2,
	right = 3
}

VERT_ALIGNMENT = {
	top = 1,
	center = 2,
	bot = 3
}

function TextCacheKey(str, col)
	return string.format("%s%02x%02x%02x%02x", str, col.r, col.g, col.b, col.a)
end


Font = class()

function Font:init(font)
	self.font = font
	self.cache = {}
end

function Font:print_internal(str, col)
	local key = TextCacheKey(str, col)
	if not self.cache[key] then
		local text_surface, err = self.font:renderText(str, "blended", col)
		if not text_surface then
			error("Failed to render text: "..err.."\n")
		end

		local text_texture, err = SYS_RENDERER:createTextureFromSurface(text_surface)
		if not text_texture then
			error("Failed to create texture from surface: "..err.."\n")
		end

		local _, _, w, h = text_texture:query()
		local text_image = { texture = text_texture, w = w, h = h, rotation = 0, flip = {} }
		self.cache[key] = text_image
	end

	return self.cache[key]
end

function Font:print(x, y, col, ...)
	local str = string.format(...)
	local text_image = self:print_internal(str, col)

	RenderImage(text_image, x, y)

	return text_image
end

function Font:print_centered(x, y, col, ...)
	return self:print_aligned(x, y, col, HORZ_ALIGNMENT.center, VERT_ALIGNMENT.top, ...)
end

function Font:print_aligned(x, y, col, horz_align, vert_align, ...)
	local str = string.format(...)
	local text_image = self:print_internal(str, col)

	local dst_x, dst_y
	if		horz_align == HORZ_ALIGNMENT.left then
		dst_x = x
	elseif	horz_align == HORZ_ALIGNMENT.center then
		dst_x = x - math.floor(text_image.w / 2)
	elseif	horz_align == HORZ_ALIGNMENT.right then
		dst_x = x - text_image.w
	end

	if		vert_align == VERT_ALIGNMENT.top then
		dst_y = y
	elseif	vert_align == VERT_ALIGNMENT.center then
		dst_y = y - math.floor(text_image.h / 2)
	elseif	vert_align == VERT_ALIGNMENT.bot then
		dst_y = y - text_image.h
	end

	RenderImage(text_image, dst_x, dst_y)

	return text_image
end
