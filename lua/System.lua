--[[-----------------------------------------------------------------------------------------------

Public functions:
	___________________

	SystemInit()		Set up SDL, system variables etc...
	- In parameters:
		w				Width of the window
		h				Height of the window
	- Returns			nothing
	___________________

	SystemQuit()		Shut down SDL
	- In parameters:	none
	- Returns			nothing
	___________________

	SystemUpdate()		Will poll all events and ask game to process them, makes sure the screen is
						rendered, lock frame rate to 60 FPS, and check how many seconds have passed
						since last SystemUpdate
	- In parameters:
						none
	- Returns
		1: boolean that if true should stop game execution, otherwise game should continue
		2: float value for how many seconds have passed since last SystemUpdate
	___________________

	class()				Sets up metatables for class hierarchy. Add an Init function to your class
						if you want to set default variables etc.
	- In parameters:
		super			The super class for this class. Nothing if class should have no super class
	- Returns
		1: A table that can be used as a class
	___________________

	Image()				A function that will load an image from disk and rotate/flip
						it according to input parameters
	- In parameters:
		file_name		A string containing the file name, excluding path. "wall_straight.png" for example
		rotation		An integer that is the rotation in degrees. Allowed rotations are 0, 90, 180, 270
		flip			A string continaing "H" for horizontal flip and "V" for vertical flip. Can be "HV"
	- Returns
		1: The SDL_Texture created, or nil of image couldn't be found on disk
	___________________

-----------------------------------------------------------------------------------------------]]--

SDL			= require "SDL"
SDL_image	= require "SDL.image"
SDL_ttf		= require "SDL.ttf"

local SYS_WINDOW = nil
SYS_RENDERER = nil
local SYS_IMAGE_CACHE = {}
local SYS_LAST_TIME = nil

--
function SystemInit(w, h)
	-- Initialize SDL
	local ret, err = SDL.init { SDL.flags.Everything }
	if not ret then
		error("Couldn't initialize SDL: "..err.."\n")
		os.exit(1)
	end

	local ret, err = SDL_image.init { SDL_image.flags.PNG }
	if not ret then
		error("Couldn't initialize SDL_image: "..err.."\n")
		os.exit(1)
	end

	local ret, err = SDL_ttf.init()
	if not ret then
		error("Couldn't initialize SDL_ttf: "..err.."\n")
		os.exit(1)
	end

	SYS_WINDOW, err = SDL.createWindow {
		title	= "Whakman",
		width	= w,
		height	= h
	}

	if not SYS_WINDOW then
		error("Couldn't create the window: "..err.."\n")
		os.exit(1)
	end

	-- Direct3D has a weird bug with rotation on some drivers
	SDL.setHint("SDL_RENDER_DRIVER", "opengl")

	SYS_RENDERER, err = SDL.createRenderer(SYS_WINDOW, -1, { SDL.rendererFlags.Accelerated, SDL.rendererFlags.PresentVSYNC })
	if not SYS_RENDERER then
		error("Couldn't create a renderer: "..err.."\n")
		os.exit(1)
	end

	local ret, err = SYS_RENDERER:setDrawBlendMode(SDL.blendMode.Blend)
	if not ret then
		error("Couldn't set draw blend mode: "..err.."\n")
		os.exit(1)
	end
end

--
function SystemQuit()
	SDL.quit()
end

--
function ImageCacheKey(file_name, rotation, flip)
	-- Give default parameters of none were provided
	rotation = rotation or 0
	flip = flip or ""

	-- Key for image wall_straight.png with horizontal flip and 270 deg rotation will be "wall_straight.png:270:H"
	local key = file_name
	key = key .. ":" .. tostring(rotation)
	if string.find(flip, "H") then key = key..":H" end
	if string.find(flip, "V") then key = key..":V" end

	return key
end

-- Class system
function class(...)
	local super = ...
	if select('#', ...) >= 1 and super == nil then
		error("trying to inherit from nil", 2)
	end
    local class_table = {}
    
    class_table.super = super
    class_table.__index = class_table
    setmetatable(class_table, super)
    
    class_table.new = function (klass, ...)
        local object = {}
        setmetatable(object, class_table)
        if object.init then
            object:init(...)
        end
        return object
    end
    return class_table
end

--
function Image(file_name, rotation, flip)
	rotation = rotation or 0

	if rotation < 0 or rotation > 270 or rotation % 90 ~= 0 then
		error("Rotation should be a multiple of 90 between 0 and 270", 2)
	end

	local key = ImageCacheKey(file_name)
	local img = SYS_IMAGE_CACHE[key]

	if not img then
		-- Image not in cache. Load it from disk and stick it in cache.
		local surface, err = SDL_image.load("images/"..file_name)
		if surface then
			img, err = SYS_RENDERER:createTextureFromSurface(surface)
			if img then
				SYS_IMAGE_CACHE[key] = img
			else
				error("Failed to create texture from surface: "..err.."\n")
				return nil
			end
		else
			-- Failed to load image from disk.
			print("Failed to load image '"..file_name.."': "..err)
			return nil
		end
	end

	-- Destination width and height
	local _, _, w, h = img:query()

	--[[
	-- Rotation (only multiples of 90)
	if rotation == 90 or rotation == 270 then
		w, h = h, w
	end
	--]]

	local flip_value = {}
	if flip then
		if string.find(flip, "H") then
			table.insert(flip_value, SDL.rendererFlip.Horizontal)
		end
		if string.find(flip, "V") then
			table.insert(flip_value, SDL.rendererFlip.Vertical)
		end
	end

	return { texture = img, w = w, h = h, rotation = rotation, flip = flip_value }
end

--
function RenderImage(img, x, y)
	local dst_rect = { x = math.floor(x), y = math.floor(y), w = img.w, h = img.h }

	SYS_RENDERER:copyEx{ texture = img.texture, destination = dst_rect, angle = img.rotation, flip = img.flip }
end

--
function SystemUpdate()
	SYS_LAST_TIME = SYS_LAST_TIME or SDL.getTicks()

	-- Handle all events for this frame
	local stop_game = false
	for e in SDL.pollEvent() do
		stop_game = HandleEvent(e) or stop_game
	end

	collectgarbage()

	-- Draw screen
	SYS_RENDERER:present()

	-- Lock frame rate to 60 fps and check how much time has passed since last update
	local time_now = SDL.getTicks()
	local time_passed = time_now - SYS_LAST_TIME
	--[[
	local wait_time = math.floor((1000/60) - time_passed)
	if wait_time > 0 then
		SDL.delay(wait_time)
	end
	time_now = SDL.getTicks()
	time_passed = time_now - SYS_LAST_TIME
	--]]
	SYS_LAST_TIME = time_now
	local delta_time = time_passed / 1000

	-- Return values
	return stop_game, delta_time
end
