---------------------------------------------------------------------------------------------------

-- Common things between Whakman and Hackman, inherits from Actor

WhakmanCommon = class(Actor)

-- Possible states
WhakmanCommon.STATES = {
	stand = 1,
	move = 2,
	eat = 3
}

-- Mouth flipping interval depending on the state, in seconds
WhakmanCommon.MOUTH_TIMER = {
	[WhakmanCommon.STATES.move] = 0.3,
	[WhakmanCommon.STATES.eat] = 0.15
}

-- Movement speed, pixels per second
WhakmanCommon.MOVE_SPEED = 120

-- images: a 2-dimensional array, 1st index being the direction, 2nd index is 1 for closed mouth, 2 for open mouth
function WhakmanCommon:init(images)
	self.direction = DIRECTIONS.right
	Actor.init(self, images[self.direction][1], true)
	self.images = images
	self.image_index = 1
	self.mouth_timer = 0
	self.state = WhakmanCommon.STATES.stand
	self.radius = (TILE_SIZE / 2) - TILE_BOUNDARY
	self.move_speed = WhakmanCommon.MOVE_SPEED

	-- Array of path segments to follow
	self.path = {}

	-- Coins and rocks collected
	self.score = 0
	self.rocks = 0

	-- Coins and rocks collected at the start of the level (to revert it if Whakman dies)
	self.start_score = 0
	self.start_rocks = 0

	-- Flags for entering and exiting the level
	-- entering: character automatically moves to the starting map position
	self.entering = false

	-- exiting: the game will switch to the next level upon reaching the right bound of the level
	self.exiting = false
end

function WhakmanCommon:update(dt)
	self:update_entering()
	self:update_path_movement(dt)
	self:update_collectibles()
	self:update_mouth(dt)
end

-- Revert coins and rocks collected to the values at the start of this level
function WhakmanCommon:reset_progress()
	self.score = self.start_score
	self.rocks = self.start_rocks
end

-- Initiate movement from the level entrance to the starting position on the map
function WhakmanCommon:enter_level(start_tile)
	self.start_tile = start_tile
	self.entering = true

	-- Pixel coordinates (to the left of the entrance)
	self.x, self.y = - TILE_SIZE / 2, (LEVELS[GAME.current_level].exit_y - 1) * TILE_SIZE + TILE_SIZE / 2

	-- First path segment (to properly enter the level)
	self.path = { {target_x = 1, target_y = LEVELS[GAME.current_level].exit_y, direction = DIRECTIONS.right} }

	-- Memorize the beginning coins and rocks
	self.start_score = self.score
	self.start_rocks = self.rocks
end

function WhakmanCommon:update_entering()
	-- After each segment of the path, find the next segment until the target tile is reached
	if self.entering and #self.path == 0 then
		local current_tile = GetTile(self.x, self.y)

		-- We can compare by reference because each tile is a separate table (no copies are created)
		if current_tile == self.start_tile then
			self.entering = false
			self.start_tile = nil
		else
			-- Find the next segment to reach the start tile
			self:find_path(function(tile)
				return (tile == self.start_tile)
			end)
		end
	end
end

function WhakmanCommon:update_path_movement(dt)
	-- If the path contains any segments, follow it
	if #self.path > 0 then
		-- The distance to be travelled
		--local move_speed = self.move_speed * dt
		local move_speed = self.move_speed / 60

		-- Whether to allow going beyond the center of current tile (should not allow if this is the target tile)
		local dont_pass_tile = false

		local path_seg = self.path[1]

		-- Don't make checks if we are not yet on the map
		if not (self.entering and self.x < 0) then
			local tile = GetTile(self.x, self.y)

			if path_seg.target_x == tile.tile_x and path_seg.target_y == tile.tile_y then
				-- This is the target tile, don't pass its center
				dont_pass_tile = true
			end
		end

		-- try_to_move will return true if our position changed
		if self:try_to_move(path_seg.direction, move_speed, dont_pass_tile) then
			-- Moved in that direction, so make sure the visuals are right
			self.direction = path_seg.direction
			self:set_state(WhakmanCommon.STATES.move)
			self.image = self.images[self.direction][self.image_index]
		else
			-- Didn't move: the path segment is over
			table.remove(self.path, 1)
		end
	end
end

-- Check whether we can collect the object in the current tile, if any
function WhakmanCommon:update_collectibles()
	-- Do not collect while entering the level (levels should be created so that there are no collectibles on the way from the entrance to the starting tile)
	if not self.entering then
		local tile, rel_x, rel_y = GetTile(self.x, self.y)
		-- tile might be nil if we just exited the level
		if not tile then
			return
		end

		if tile.content and (math.abs(rel_x - TILE_SIZE / 2) <= self.radius and math.abs(rel_y - TILE_SIZE / 2) <= self.radius) then
			-- There is a collectible and we are within reach
			if		tile.content_type == TILE_CONTENT.coin then
				-- It's a coin, so increase our score and reduce the total number of coins on the levels
				self.score = self.score + 1
				GAME.coins = GAME.coins - 1
				if GAME.coins == 0 then
					-- No more coins on the level! The right exit will lead to the next level
					GAME.whakman.exiting = true
					GAME:show_exit_indicators()
				end
			elseif	tile.content_type == TILE_CONTENT.rock then
				-- Pick up the rock
				self.rocks = self.rocks + 1
			end

			-- Remove the collectible from the tile
			tile:remove_content()
		end
	end
end

function WhakmanCommon:update_mouth(dt)
	-- Don't animate the mouth while standing
	if self.state == WhakmanCommon.STATES.stand then
		self.mouth_timer = 0
	else
		-- Get frame interval depending on the state
		local frame_length = WhakmanCommon.MOUTH_TIMER[self.state]

		self.mouth_timer = self.mouth_timer + dt
		if self.mouth_timer >= frame_length then
			-- Enough time passed: flip the mouth
			self.mouth_timer = self.mouth_timer - frame_length
			self:flip_mouth()
		end
	end
end

-- direction: the desired direction
-- move_speed: distance to move, in pixels
-- dont_pass_tile: stop in the center of the tile even if it allows further movement
-- Returns true if any movement was done, false otherwise
function WhakmanCommon:try_to_move(direction, move_speed, dont_pass_tile)
	-- If we are to the left of the level, just increase x
	if self.entering and self.x < 0 then
		self.x = self.x + move_speed
		return true
	end

	local tile, rel_x, rel_y = GetTile(self.x, self.y)

	-- Remember the old coordinates to know if any movement was done
	local old_x, old_y = self.x, self.y
	if tile then
		if tile.available_directions[direction] then
			if direction == DIRECTIONS.up or direction == DIRECTIONS.down then
				-- Snap to horizontal center of the tile if moving up or down
				local dist_center = math.abs(rel_x - TILE_SIZE / 2)
				if dist_center > 0 and dist_center < TILE_SNAP_GAP then
					self.x = self.x + TILE_SIZE / 2 - rel_x
					rel_x = TILE_SIZE / 2
				end
			else
				-- Snap to vertical center of the tile if moving left or right
				local dist_center = math.abs(rel_y - TILE_SIZE / 2)
				if dist_center > 0 and dist_center < TILE_SNAP_GAP then
					self.y = self.y + TILE_SIZE / 2 - rel_y
					rel_y = TILE_SIZE / 2
				end
			end
		end

		-- The visual boundaries of the tile
		local bound_l, bound_r, bound_t, bound_b = TILE_BOUNDARY, TILE_SIZE - TILE_BOUNDARY, TILE_BOUNDARY, TILE_SIZE - TILE_BOUNDARY

		-- Do not allow movement beyond the bound if we are not centered or there is no path or if we don't want to pass this tile
		if		direction == DIRECTIONS.up then
			local new_rel_y = rel_y - move_speed
			if new_rel_y - self.radius < bound_t and (rel_x - self.radius < bound_l or rel_x + self.radius > bound_r or not tile.available_directions[direction] or dont_pass_tile) then
				new_rel_y = bound_t + self.radius
			end

			self.y = self.y + new_rel_y - rel_y
		elseif	direction == DIRECTIONS.right then
			local new_rel_x = rel_x + move_speed
			if new_rel_x + self.radius > bound_r and (rel_y - self.radius < bound_t or rel_y + self.radius > bound_b or not tile.available_directions[direction] or dont_pass_tile) then
				new_rel_x = bound_r - self.radius
			end

			self.x = self.x + new_rel_x - rel_x
		elseif	direction == DIRECTIONS.down then
			local new_rel_y = rel_y + move_speed
			if new_rel_y + self.radius > bound_b and (rel_x - self.radius < bound_l or rel_x + self.radius > bound_r or not tile.available_directions[direction] or dont_pass_tile) then
				new_rel_y = bound_b - self.radius
			end

			self.y = self.y + new_rel_y - rel_y
		elseif	direction == DIRECTIONS.left then
			local new_rel_x = rel_x - move_speed
			if new_rel_x - self.radius < bound_l and (rel_y - self.radius < bound_t or rel_y + self.radius > bound_b or not tile.available_directions[direction] or dont_pass_tile) then
				new_rel_x = bound_l + self.radius
			end

			self.x = self.x + new_rel_x - rel_x
		end
	end

	-- Should only be possible when going through the tunnel
	if self.x < 0 then
		-- Teleport to the other side of the level
		self.x = self.x + GAME.game_area.w
	end

	if self.x >= GAME.game_area.w then
		if self.exiting then
			-- Reached the exit: go to the next level
			GAME.fade_surface:fade_in(function()
				GAME:load_level(GAME.current_level + 1)
				GAME.fade_surface:fade_out()
			end)
		else
			-- Going through the tunnel: teleport to the other side
			self.x = self.x - GAME.game_area.w
		end
	end

	-- Check if any movement was done
	return self.x ~= old_x or self.y ~= old_y
end

-- is_target_tile: a function that takes a Tile object as the argument and returns true if that tile qualifies as our target
-- Inserts the first segment of the found path into self.path
-- The condition for the target tile should be such that at least one tile qualifies
-- The starting position should not qualify
function WhakmanCommon:find_path(is_target_tile)
	local start_tile = GetTile(self.x, self.y)

	-- 2-dimensional array that stores which tiles are visited during the search
	local visited_tiles = {}

	-- The queue for BFS
	local queue = { { x = start_tile.tile_x, y = start_tile.tile_y, direction = nil } }
	local head = 1
	local tail = 2

	-- BFS that stops upon finding the first target tile
	while head < tail do
		local position = queue[head]
		head = head + 1

		-- Mark the tile as visited
		if not visited_tiles[position.y] then
			visited_tiles[position.y] = {}
		end
		visited_tiles[position.y][position.x] = true

		-- Get the tile object and check if it qualifies
		local tile = GAME.tiles[position.y][position.x]
		if is_target_tile(tile) then
			break
		end

		-- Try to go in every direction if possible
		for _, direction in pairs(DIRECTIONS) do
			if tile.available_directions[direction] then
				-- direction is nil for the very first tile, so the new position will store the first direction that was taken to get there
				local new_position = { x = position.x, y = position.y, direction = position.direction or direction }

				if		direction == DIRECTIONS.up then
					new_position.y = new_position.y - 1
				elseif	direction == DIRECTIONS.right then
					new_position.x = new_position.x + 1

					-- Allow going through the tunnel
					if new_position.x > LEVEL_WIDTH and new_position.y == LEVELS[GAME.current_level].exit_y then
						new_position.x = 1
					end
				elseif	direction == DIRECTIONS.down then
					new_position.y = new_position.y + 1
				elseif	direction == DIRECTIONS.left then
					new_position.x = new_position.x - 1

					-- Allow going through the tunnel
					if new_position.x < 1 and new_position.y == LEVELS[GAME.current_level].exit_y then
						new_position.x = LEVEL_WIDTH
					end
				end

				-- If the new position is on the map and not visited then insert it in the queue
				if new_position.x >= 1 and new_position.x <= LEVEL_WIDTH
				and new_position.y >= 1 and new_position.y <= LEVEL_HEIGHT
				and (not visited_tiles[new_position.y] or not visited_tiles[new_position.y][new_position.x]) then
					table.insert(queue, new_position)
					tail = tail + 1
				end
			end
		end
	end

	-- First direction that was taken
	local found_direction = queue[head - 1].direction

	if found_direction then
		local end_x, end_y = start_tile.tile_x, start_tile.tile_y

		-- Go in that direction until either the target tile is reached or there is a crossroads or the direction is not allowed anymore
		repeat
			if		found_direction == DIRECTIONS.up then
				end_y = end_y - 1
			elseif	found_direction == DIRECTIONS.right then
				end_x = end_x + 1

				-- Go through the tunnel
				if end_x > LEVEL_WIDTH then
					end_x = 1
				end
			elseif	found_direction == DIRECTIONS.down then
				end_y = end_y + 1
			elseif	found_direction == DIRECTIONS.left then
				end_x = end_x - 1

				-- Go through the tunnel
				if end_x < 1 then
					end_x = LEVEL_WIDTH
				end
			end

			local tile = GAME.tiles[end_y][end_x]

			-- Check if this is the target tile
			if is_target_tile(tile) then
				break
			end

			-- Check if this direction is no longer allowed
			if not tile.available_directions[found_direction] then
				break
			end

			-- Check if we reached a crossroads
			if ((found_direction == DIRECTIONS.up or found_direction == DIRECTIONS.down)
			and (tile.available_directions[DIRECTIONS.right] or tile.available_directions[DIRECTIONS.left]))
			or ((found_direction == DIRECTIONS.right or found_direction == DIRECTIONS.left)
			and (tile.available_directions[DIRECTIONS.up] or tile.available_directions[DIRECTIONS.down])) then
				break
			end
		until false

		-- Insert the path segment
		table.insert(self.path, { target_x = end_x, target_y = end_y, direction = found_direction })
	end
end

function WhakmanCommon:flip_mouth()
	-- image_index is 1 for closed mouth and 2 for open mouth
	self.image_index = (self.image_index % 2) + 1
	self.image = self.images[self.direction][self.image_index]
end

-- Set state and refresh the image to have the right direction and mouth position
function WhakmanCommon:set_state(new_state)
	if new_state == WhakmanCommon.STATES.move and self.state == WhakmanCommon.STATES.stand then
		self.image_index = 2
	elseif new_state == WhakmanCommon.STATES.stand then
		self.image_index = 1
	end
	self.image = self.images[self.direction][self.image_index]

	self.state = new_state
end

function WhakmanCommon:draw(offset_x, offset_y)
	-- Draw a copy of yourself when going through the tunnel
	if self.x < TILE_SIZE / 2 and not self.entering then
		Actor.draw(self, offset_x + GAME.game_area.w, offset_y)
	end

	if self.x >= GAME.game_area.w - TILE_SIZE / 2 and not self.exiting then
		Actor.draw(self, offset_x - GAME.game_area.w, offset_y)
	end

	Actor.draw(self, offset_x, offset_y)
end
