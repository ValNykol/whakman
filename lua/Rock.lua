---------------------------------------------------------------------------------------------------

-- Rock thrown by Whakman, inherits from Actor

Rock = class(Actor)

-- Rock's movement speed, pixels per second
Rock.MOVE_SPEED = 240

-- Rock's crumbling animation frame interval, in seconds
Rock.CRUMBLE_FRAME_LENGTH = 0.1

-- x, y: Whakman's coordinates
-- direction: Whakman's current direction
function Rock:init(x, y, direction)
	-- The crumble frames are played through upon hitting an obstacle, after which the rock is removed
	self.images = {
		Image("rock.png"),
		Image("crumble_01.png"),
		Image("crumble_02.png"),
		Image("crumble_03.png"),
		Image("crumble_04.png")
	}

	self.image_index = 1
	Actor.init(self, self.images[self.image_index], true)
	self.x = x
	self.y = y
	self.direction = direction

	-- Flag for whether the rock hit an obstacle
	self.crumbling = false

	-- Animation timer, in seconds
	self.crumble_timer = 0

	-- Flag for whether the rock should be removed (after it's done with crumble animation)
	self.should_remove = false
end

function Rock:update(dt)
	if self.crumbling then
		-- Rock is crumbling: play through the animation and then set should_remove
		self.crumble_timer = self.crumble_timer + dt
		if self.crumble_timer >= Rock.CRUMBLE_FRAME_LENGTH then
			self.crumble_timer = self.crumble_timer - Rock.CRUMBLE_FRAME_LENGTH
			self.image_index = self.image_index + 1

			if self.image_index > #self.images then
				self.should_remove = true
			else
				self.image = self.images[self.image_index]
			end
		end
	else
		-- Rock is flying: move in the direction until reaching a wall or Hackman
		local move_speed = Rock.MOVE_SPEED * dt
		local tile, rel_x, rel_y = GetTile(self.x, self.y)

		if		self.direction == DIRECTIONS.up then
			local new_rel_y = rel_y - move_speed
			if new_rel_y < TILE_BOUNDARY and not tile.available_directions[self.direction] then
				self:crumble()
			else
				self.y = self.y - move_speed
			end
		elseif	self.direction == DIRECTIONS.right then
			local new_rel_x = rel_x + move_speed
			if new_rel_x > TILE_SIZE - TILE_BOUNDARY and (not tile.available_directions[self.direction] or tile.tile_x == LEVEL_WIDTH) then
				self:crumble()
			else
				self.x = self.x + move_speed
			end
		elseif	self.direction == DIRECTIONS.down then
			local new_rel_y = rel_y + move_speed
			if new_rel_y > TILE_SIZE - TILE_BOUNDARY and not tile.available_directions[self.direction] then
				self:crumble()
			else
				self.y = self.y + move_speed
			end
		elseif	self.direction == DIRECTIONS.left then
			local new_rel_x = rel_x - move_speed
			if new_rel_x < TILE_BOUNDARY and (not tile.available_directions[self.direction] or tile.tile_x == 1) then
				self:crumble()
			else
				self.x = self.x - move_speed
			end
		end

		if not self.crumbling then
			-- Check if we collide with Hackman
			if math.abs(self.x - GAME.hackman.x) <= GAME.hackman.radius and math.abs(self.y - GAME.hackman.y) <= GAME.hackman.radius then
				self:crumble()
				GAME.hackman:stun()
			end
		end
	end
end

-- Called upon hitting a wall or Hackman
function Rock:crumble()
	self.crumbling = true
	self.crumble_timer = 0
end
