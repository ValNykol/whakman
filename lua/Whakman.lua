require "Rock"

---------------------------------------------------------------------------------------------------

-- Whakman, the playable character, inherits from WhakmanCommon

Whakman = class(WhakmanCommon)

function Whakman:init(images)
	WhakmanCommon.init(self, images)

	-- Array for storing thrown rocks
	self.flying_rocks = {}

	-- Flag for when the rock-throwing key is held for several frames
	self.just_threw_rock = false

	-- Flag to indicate if Whakman has been eaten by Hackman
	self.dead = false

	-- Lives: the game is lost upon reaching 0
	self.lives = 3
end

function Whakman:update(dt)
	-- Don't update during the death animation (Hackman takes care of that)
	if not self.dead then
		-- Disable input during the enter level animation
		if not self.entering then
			self:update_keys(dt)
		end

		-- Update the flying rocks
		self:update_rocks(dt)

		-- Call the update of the base class
		WhakmanCommon.update(self, dt)
	end
end

function Whakman:update_keys(dt)
	-- Distance to be travelled in pixels
	--local move_speed = WhakmanCommon.MOVE_SPEED * dt
	local move_speed = WhakmanCommon.MOVE_SPEED / 60

	-- Whether any actual movement was done
	local moving = false

	if		KEYS[SDL.key.Up] and not KEYS[SDL.key.Down] then
		local moved = self:try_to_move(DIRECTIONS.up, move_speed)
		if moved then
			self.direction = DIRECTIONS.up
			moving = true
		end
	elseif	KEYS[SDL.key.Down] and not KEYS[SDL.key.Up] then
		local moved = self:try_to_move(DIRECTIONS.down, move_speed)
		if moved then
			self.direction = DIRECTIONS.down
			moving = true
		end
	end

	if not moving then
		if KEYS[SDL.key.Left] and not KEYS[SDL.key.Right] then
			local tile = GetTile(self.x, self.y)
			local dont_pass_tile = false

			-- Disable going through the left tunnel if all the coins have been collected
			if tile.tile_x == 1 and self.exiting then
				dont_pass_tile = true
			end

			local moved = self:try_to_move(DIRECTIONS.left, move_speed, dont_pass_tile)
			if moved then
				self.direction = DIRECTIONS.left
				moving = true
			end
		elseif KEYS[SDL.key.Right] and not KEYS[SDL.key.Left] then
			local moved = self:try_to_move(DIRECTIONS.right, move_speed)
			if moved then
				self.direction = DIRECTIONS.right
				moving = true
			end
		end
	end

	if KEYS[SDL.key.Space] then
		-- Throw a rock in the direction we are facing
		if not self.just_threw_rock and self.rocks > 0 then
			-- Mark the flag to not throw more rocks until the key is released
			self.just_threw_rock = true

			-- Reduce the available rocks
			self.rocks = self.rocks - 1

			-- Create the flying rock
			local rock = Rock:new(self.x, self.y, self.direction)
			table.insert(self.flying_rocks, rock)
		end
	else
		-- Unmark the flag so we can throw a rock upon the next press of the key
		self.just_threw_rock = false
	end

	-- Set the corresponding state depending on whether any movement was done
	if moving then
		self:set_state(WhakmanCommon.STATES.move)
	else
		self:set_state(WhakmanCommon.STATES.stand)
	end

	self.image = self.images[self.direction][self.image_index]
end

function Whakman:update_rocks(dt)
	local i = 1

	-- Iterate through flying_rocks and update them while removing the ones that are destroyed
	while i <= #self.flying_rocks do
		local rock = self.flying_rocks[i]
		rock:update(dt)
		if rock.should_remove then
			table.remove(self.flying_rocks, i)
		else
			i = i + 1
		end
	end
end

function Whakman:enter_level(start_tile)
	-- Call enter_level of the base class
	WhakmanCommon.enter_level(self, start_tile)

	-- Reset the flying rocks
	self.flying_rocks = {}

	-- Reset the flags
	self.exiting = false
	self.dead = false
end

function Whakman:draw(offset_x, offset_y)
	-- Call draw of the base class
	WhakmanCommon.draw(self, offset_x, offset_y)

	-- Draw all the thrown rocks
	for _, rock in ipairs(self.flying_rocks) do
		rock:draw(offset_x, offset_y)
	end
end
