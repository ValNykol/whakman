require "Birds"

---------------------------------------------------------------------------------------------------

-- Hackman, AI-controlled nemesis of Whakman, inherits from WhakmanCommon

Hackman = class(WhakmanCommon)

-- If Hackman moves for a time without collecting coins, his speed increases
-- Duration over which the speed increases, in seconds
Hackman.SPEED_UP_TIME = 3
-- Maximum speed multiplier
Hackman.SPEED_UP_MULTIPLIER = 1.5

-- Delay before entering the level after Whakman, in seconds
Hackman.ENTER_LEVEL_DELAY = 2

-- Delay when stunned by a rock
Hackman.STUN_DELAY = 3

-- States for Whakman's death animation
Hackman.EAT_WHAKMAN_STATES = {
	move = 1,
	digest = 2,
	spit = 3,
	wait = 4
}

-- Movement speed for eating Whakman, pixels per second
Hackman.EAT_MOVE_SPEED = 60

-- How long to digest Whakman, in seconds
Hackman.AFTER_EAT_DELAY = 1

-- How fast Whakman's skull is moving, pixels per second
Hackman.SKULL_MOVE_SPEED = 120

-- Duration of Whakman's skull movement
Hackman.SKULL_MOVE_DURATION = 0.5

-- Duration of waiting before the fade out and the level is restarted
Hackman.AFTER_SKULL_DELAY = 1

function Hackman:init(images)
	WhakmanCommon.init(self, images)

	-- Time to wait before doing anything, in seconds
	self.delay = 0

	-- Birds above Hackman's head when stunned
	self.birds = nil

	-- State indicator for Whakman's death animation
	self.eat_whakman_state = nil

	-- Time passed without collecting a coin, in seconds
	self.speed_up_timer = 0
end

function Hackman:update(dt)
	if self.delay > 0 then
		self.delay = self.delay - dt

		-- Update the birds if stunned
		if self.birds then
			if self.delay > 0 then
				self.birds:update(dt)
			else
				-- Delay is over, remove the birds
				self.birds = nil
			end
		end
	else
		if #self.path == 0 and not self.entering then
			if not GAME.whakman.dead then
				-- Don't have a path segment to follow: find a new one
				self:find_closest_target()
			else
				-- Whakman's death animation
				self:update_eat_whakman(dt)
			end
		end

		-- Don't speed up during the enter level animation
		if not self.entering then
			self:update_speed_up(dt)
		end

		-- Memorize the score before calling the update of the base class to see if any coins were collected
		local score_before = self.score
		WhakmanCommon.update(self, dt)
		if self.score > score_before then
			-- A coin was collected: reset the speed
			self:reset_speed_up()
		end
	end
end

-- Should be called once upon reaching close enough distance to Whakman
function Hackman:on_eat_whakman()
	self.should_change_direction = true

	-- We should finish our path only if we need to change the direction before eating Whakman (e.g. when Hackman is moving right and Whakman is above)
	if ((self.direction == DIRECTIONS.up or self.direction == DIRECTIONS.down)
	and (self.x == GAME.whakman.x))
	or ((self.direction == DIRECTIONS.right or self.direction == DIRECTIONS.left)
	and (self.y == GAME.whakman.y)) then
		self.should_change_direction = false
		self.path = {}
	end
end

function Hackman:update_eat_whakman(dt)
	if not self.eat_whakman_state then
		self.eat_whakman_state = Hackman.EAT_WHAKMAN_STATES.move
	end

	if self.eat_whakman_state == Hackman.EAT_WHAKMAN_STATES.move then
		-- First, move close enough to overlap with Whakman
		local move_speed = Hackman.EAT_MOVE_SPEED * dt
		self:set_state(WhakmanCommon.STATES.eat)

		if math.abs(self.x - GAME.whakman.x) <= move_speed and math.abs(self.y - GAME.whakman.y) <= move_speed then
			-- We are close enough to overlap: change Whakman's image into a skull
			self.x = GAME.whakman.x
			self.y = GAME.whakman.y
			self.eat_whakman_state = Hackman.EAT_WHAKMAN_STATES.digest
			self:set_state(WhakmanCommon.STATES.stand)
			GAME.whakman.image = Image("skull.png")

			-- Remove the exiting flag to be able to spit the skull through the tunnel
			GAME.whakman.exiting = false
			self.digest_timer = Hackman.AFTER_EAT_DELAY
		else
			if self.should_change_direction then
				-- We arrived at Whakman's tile, so we can move towards him in a straight line
				self.should_change_direction = false

				local whak_x, whak_y = GAME.whakman.x, GAME.whakman.y
				if		self.y > whak_y then
					self.direction = DIRECTIONS.up
				elseif	self.x < whak_x then
					self.direction = DIRECTIONS.right
				elseif	self.y < whak_y then
					self.direction = DIRECTIONS.down
				elseif	self.x > whak_x then
					self.direction = DIRECTIONS.left
				end
			end

			self:try_to_move(self.direction, move_speed)
		end
	elseif self.eat_whakman_state == Hackman.EAT_WHAKMAN_STATES.digest then
		-- Wait until Whakman is digested
		self.digest_timer = self.digest_timer - dt
		if self.digest_timer <= 0 then
			self.digest_timer = nil
			self.image_index = 2
			self.image = self.images[self.direction][self.image_index]
			self.eat_whakman_state = Hackman.EAT_WHAKMAN_STATES.spit
			self.spit_timer = Hackman.SKULL_MOVE_DURATION
		end
	elseif self.eat_whakman_state == Hackman.EAT_WHAKMAN_STATES.spit then
		-- Spit the skull out
		local move_speed = Hackman.SKULL_MOVE_SPEED * dt
		self.spit_timer = self.spit_timer - dt
		if self.spit_timer <= 0 then
			self.spit_timer = nil
			self.eat_whakman_state = Hackman.EAT_WHAKMAN_STATES.wait
			self.wait_timer = Hackman.AFTER_SKULL_DELAY
		else
			-- Move Whakman('s skull) in the direction Hackman is facing, ignoring collision
			if		self.direction == DIRECTIONS.up then
				GAME.whakman.y = GAME.whakman.y - move_speed
			elseif	self.direction == DIRECTIONS.right then
				GAME.whakman.x = GAME.whakman.x + move_speed
			elseif	self.direction == DIRECTIONS.down then
				GAME.whakman.y = GAME.whakman.y + move_speed
			elseif	self.direction == DIRECTIONS.left then
				GAME.whakman.x = GAME.whakman.x - move_speed
			end
		end
	elseif self.eat_whakman_state == Hackman.EAT_WHAKMAN_STATES.wait then
		-- Wait before restarting the level (or showing the end screen)
		self.wait_timer = self.wait_timer - dt
		if self.wait_timer <= 0 then
			self.wait_timer = nil
			GAME.fade_surface:fade_in(function()
				GAME.whakman.lives = GAME.whakman.lives - 1
				if GAME.whakman.lives > 0 then
					GAME.whakman:reset_progress()
					GAME.hackman:reset_progress()
				end
				GAME:load_level(GAME.current_level)
				GAME.fade_surface:fade_out()
			end)
		end
	end
end

function Hackman:update_speed_up(dt)
	-- Increase the timer, capped at SPEED_UP_TIME
	self.speed_up_timer = self.speed_up_timer + dt
	if self.speed_up_timer > Hackman.SPEED_UP_TIME then
		self.speed_up_timer = Hackman.SPEED_UP_TIME
	end

	self:refresh_move_speed()
end

-- Reset the movement speed to the starting speed
function Hackman:reset_speed_up()
	self.speed_up_timer = 0
	self:refresh_move_speed()
end

-- Linearly interpolate the speed between the starting and maximum speeds based on the time passed without collecting coins
function Hackman:refresh_move_speed()
	local multiplier = 1 + (self.speed_up_timer / Hackman.SPEED_UP_TIME) * (Hackman.SPEED_UP_MULTIPLIER - 1)
	self.move_speed = multiplier * WhakmanCommon.MOVE_SPEED
end

function Hackman:enter_level(start_tile)
	-- Call enter_level of the base class
	WhakmanCommon.enter_level(self, start_tile)

	-- Set the delay before entering the level
	self.delay = Hackman.ENTER_LEVEL_DELAY

	-- Remove the stunned birds in case they were there
	self.birds = nil

	-- Reset Whakman's death animation state
	self.eat_whakman_state = nil

	-- Set movement speed to the starting speed
	self:reset_speed_up()
end

-- Find the path segment that leads to either collecting a coin or eating Whakman
function Hackman:find_closest_target()
	local whak_tile = GetTile(GAME.whakman.x, GAME.whakman.y)

	WhakmanCommon.find_path(self, function(tile)
		if tile == whak_tile or tile.content_type == TILE_CONTENT.coin then
			return true
		end
		return false
	end)
end

-- Stun Hackman and create birds above his head
function Hackman:stun()
	self.delay = Hackman.STUN_DELAY
	self:set_state(WhakmanCommon.STATES.stand)
	self.birds = Birds:new(self.direction)

	-- Reset the speed upon getting stunned
	self:reset_speed_up()
end

function Hackman:draw(offset_x, offset_y)
	WhakmanCommon.draw(self, offset_x, offset_y)

	-- Draw the birds above Hackman's head when stunned
	if self.birds then
		self.birds:draw(self.x + offset_x, self.y + offset_y)
	end
end
