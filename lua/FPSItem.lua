---------------------------------------------------------------------------------------------------

-- FPS item shows current FPS

FPSItem = class()

function FPSItem:init(font, color, x, y)
	self.font = font
	self.color = color
	self.x = x
	self.y = y

	self.fps = 0
end

function FPSItem:draw()
	if self.font then
		self.font:print(self.x, self.y, self.color, "FPS: "..tostring(math.floor(self.fps)))
	end
end

function FPSItem:update(dt)
	-- Calculate the FPS as a smoothed average
	local newFPS = 0
	if dt > 0 then
		newFPS = 1 / dt
	end

	if self.fps == 0 then
		self.fps = newFPS
	elseif newFPS > 0 then
		self.fps = 0.9 * self.fps + 0.1 * newFPS
	end
end
