---------------------------------------------------------------------------------------------------

-- ExitIndicator is a blinking indicator to show the player where to go after collecting all the coins, inherits from Actor

ExitIndicator = class(Actor)

-- Interval after which the indicator's visibility will alternate, in seconds
ExitIndicator.BLINK_DURATION = 0.5

-- The total duration of the indicator's existence, in seconds
ExitIndicator.TOTAL_DURATION = 3

function ExitIndicator:init(image)
	Actor.init(self, image)

	-- Flag for whether the indicator should be removed (the total duration has passed)
	self.should_remove = false

	-- Whether to draw the indicator
	self.visible = true

	-- Timer for indicator's blinking
	self.blink_timer = 0

	-- Timer for indicator's existence
	self.total_timer = 0
end

function ExitIndicator:update(dt)
	-- Update the blink timer and alternate visibility every BLINK_DURATION
	self.blink_timer = self.blink_timer + dt
	if self.blink_timer >= ExitIndicator.BLINK_DURATION then
		self.blink_timer = self.blink_timer - ExitIndicator.BLINK_DURATION
		self.visible = not self.visible
	end

	-- Update the total timer and set should_remove when TOTAL_DURATION has passed
	self.total_timer = self.total_timer + dt
	if self.total_timer >= ExitIndicator.TOTAL_DURATION then
		self.should_remove = true
	end
end

function ExitIndicator:draw(offset_x, offset_y)
	-- Only call draw of base class if visible
	if self.visible then
		Actor.draw(self, offset_x, offset_y)
	end
end
