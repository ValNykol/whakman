---------------------------------------------------------------------------------------------------

-- Directions

DIRECTIONS = {
	up = 1,
	right = 2,
	down = 3,
	left = 4
}

---------------------------------------------------------------------------------------------------

-- Tile images

-- This array stores all the allowed tiles and the corresponding images
TILE_IMAGES = {
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_cross.png" } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_straight.png" } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_straight.png",	rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_solid.png" } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_t.png" } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_t.png",		rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_t.png",		rotation = 180 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_t.png",		rotation = 270 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_turn.png" } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_turn.png",		rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_turn.png",		rotation = 180 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_turn.png",		rotation = 270 } },
	{ available_directions = {[DIRECTIONS.up] = true,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_end.png" } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = true,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_end.png",		rotation = 90 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = true,	[DIRECTIONS.left] = false	}, image_params = { file = "wall_end.png",		rotation = 180 } },
	{ available_directions = {[DIRECTIONS.up] = false,	[DIRECTIONS.right] = false,	[DIRECTIONS.down] = false,	[DIRECTIONS.left] = true	}, image_params = { file = "wall_end.png",		rotation = 270 } }
}

-- Should be called at the start (after SDL is initialized) to cache all the tile images
function InitTileImages()
	for _, tile_image in ipairs(TILE_IMAGES) do
		tile_image.image = Image(tile_image.image_params.file, tile_image.image_params.rotation, tile_image.image_params.flip)
	end
end

-- available_directions: an array with directions as indices and values true or false (indicating the allowed movement from this tile)
-- Returns the corresponding image
function GetTileImage(available_directions)
	for _, tile_image in ipairs(TILE_IMAGES) do
		local is_match = true
		for dir, available in pairs(available_directions) do
			if available ~= tile_image.available_directions[dir] then
				is_match = false
				break
			end
		end

		if is_match then
			return tile_image.image
		end
	end
end

---------------------------------------------------------------------------------------------------

-- Content images

-- This array stores the images for the collectibles
CONTENT_IMAGES = {
	[TILE_CONTENT.coin]	= { image_params = { file = "coin_small.png" } },
	[TILE_CONTENT.rock]	= { image_params = { file = "rock.png" } }
}

-- Should be called at the start (after SDL is initialized) to cache all the content images
function InitContentImages()
	for _, content_image in pairs(CONTENT_IMAGES) do
		content_image.image = Image(content_image.image_params.file, content_image.image_params.rotation, content_image.image_params.flip)
	end
end

-- content_type: the character representing the type of the collectible object
function CreateContentActor(content_type)
	local content_actor
	if		content_type == TILE_CONTENT.coin then
		content_actor = Actor:new(CONTENT_IMAGES[content_type].image, true, true)
	elseif	content_type == TILE_CONTENT.rock then
		content_actor = Actor:new(CONTENT_IMAGES[content_type].image, true, true)
	end
	return content_actor
end

---------------------------------------------------------------------------------------------------

-- Tile size in pixels
TILE_SIZE = 64

-- Distance from the edge of the tile to the visual "wall", in pixels
TILE_BOUNDARY = 10

-- Distance from the center of a tile that Whakman can "cut" when changing directions, in pixels
TILE_SNAP_GAP = 16

Tile = class(Actor)

function Tile:init(available_directions)
	self.available_directions = available_directions
	local image = GetTileImage(available_directions)
	Actor.init(self, image)

	self.x = 0
	self.y = 0
end

-- Create and attach a collectible object to this tile
function Tile:add_content(content_type)
	self.content = CreateContentActor(content_type)
	self.content_type = content_type

	-- Relative position of the collectible
	self.content.x = TILE_SIZE / 2
	self.content.y = TILE_SIZE / 2
end

-- Remove the collectible object from this tile (or nothing if no collectible was attached)
function Tile:remove_content()
	if self.content then
		-- Just drop the reference to the object, leave the rest to garbage collector
		self.content = nil
		self.content_type = nil
	end
end

function Tile:draw(offset_x, offset_y)
	Actor.draw(self, offset_x, offset_y)
	-- Draw the collectible object if attached
	if self.content then
		self.content:draw(self.x + offset_x, self.y + offset_y)
	end
end
