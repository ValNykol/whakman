---------------------------------------------------------------------------------------------------

-- Class for something drawn on the screen

Actor = class()
Actor.hover_phase = 0

-- "Static" method: to make sure all the hovering objects are in sync (even if created later)
function Actor.update_hover(dt)
	Actor.hover_phase = Actor.hover_phase + 8 * dt
	if Actor.hover_phase > 2 * math.pi then
		Actor.hover_phase = Actor.hover_phase - 2 * math.pi
	end
end

function Actor:init(image, centered, hover)
	self.image = image

	-- Position on screen
	self.x = 0
	self.y = 0

	-- Whether position refers to the top left corner or the center
	self.centered = centered

	-- Hover effect for collectible objects
	self.hover = hover

	if self.hover then
		-- Hover amplitude
		self.hover_amp = 2
	end
end

-- offset_x and offset_y will shift the actor's position on the screen
function Actor:draw(offset_x, offset_y)
	-- Get position on screen
	local x,y
	if self.centered then
		x = math.floor(self.x - self.image.w / 2)
		y = math.floor(self.y - self.image.h / 2)
	else
		x = self.x
		y = self.y
	end

	-- Apply the offset (if specified)
	if offset_x then
		x = x + offset_x
	end
	if offset_y then
		y = y + offset_y
	end

	-- Apply the hover effect (if applicable)
	if self.hover then
		y = y + math.floor(math.sin(Actor.hover_phase) * self.hover_amp + 0.5)
	end

	RenderImage(self.image, x, y)
end
