require "Levels"
require "Actor"
require "Tile"
require "FadeSurface"
require "WhakmanCommon"
require "Whakman"
require "Hackman"
require "OSDItem"
require "FPSItem"
require "Label"
require "ExitIndicator"

-- Global access point to the instance of Game class
GAME = nil

SCREEN_WIDTH = LEVEL_WIDTH * TILE_SIZE
SCREEN_HEIGHT = (LEVEL_HEIGHT + 1) * TILE_SIZE

Game = class()

function Game:init()
	-- Assign at the beginning so that this object can be referred to before the return from Game:new()
	GAME = self

	-- Initialize tile and content images
	InitTileImages()
	InitContentImages()

	-- Font to be used for the OSD items (and FPS item if enabled)
	local font_size = 24
	local font = LoadFont("LondonBetween.ttf", font_size)

	-- The rect of the actual game area
	self.game_area = {
		x = 0,
		y = TILE_SIZE / 2,
		w = LEVEL_WIDTH * TILE_SIZE,
		h = LEVEL_HEIGHT * TILE_SIZE
	}

	-- Create Whakman
	self.whakman = Whakman:new({
		[DIRECTIONS.up] =		{Image("whakman_01.png", 90, "HV"), Image("whakman_02.png", 90, "HV")},
		[DIRECTIONS.right] =	{Image("whakman_01.png"), Image("whakman_02.png")},
		[DIRECTIONS.down] =		{Image("whakman_01.png", 90, "V"), Image("whakman_02.png", 90, "V")},
		[DIRECTIONS.left] =		{Image("whakman_01.png", 0, "H"), Image("whakman_02.png", 0, "H")}
	})

	-- Create Hackman
	self.hackman = Hackman:new({
		[DIRECTIONS.up] =		{Image("hackman_01.png", 90, "HV"), Image("hackman_02.png", 90, "HV")},
		[DIRECTIONS.right] =	{Image("hackman_01.png"), Image("hackman_02.png")},
		[DIRECTIONS.down] =		{Image("hackman_01.png", 90, "V"), Image("hackman_02.png", 90, "V")},
		[DIRECTIONS.left] =		{Image("hackman_01.png", 0, "H"), Image("hackman_02.png", 0, "H")}
	})

	-- Create OSD items
	local col_osd = { r = 0xff, g = 0xff, b = 0xff, a = 0xff }

	-- Lives indicator
	local osd_x, osd_y = 10, SCREEN_HEIGHT - font_size - 10
	local osd_lives = OSDItem:new(Image("osd_whakman.png"), font, col_osd, "Whakmans:", osd_x, osd_y, function()
		return GAME.whakman.lives
	end)

	-- Rocks indicator
	osd_x, osd_y = SCREEN_WIDTH - 150, SCREEN_HEIGHT - font_size - 10
	local osd_rocks = OSDItem:new(Image("osd_rock.png"), font, col_osd, "Rocks:", osd_x, osd_y, function()
		return GAME.whakman.rocks
	end)

	-- Whakman score
	osd_x, osd_y = 10, 10
	local osd_whak_score = OSDItem:new(Image("osd_whak_score.png"), font, col_osd, "Score:", osd_x, osd_y, function()
		return GAME.whakman.score
	end)

	-- Hackman score
	osd_x, osd_y = SCREEN_WIDTH - 150, 10
	local osd_hack_score = OSDItem:new(Image("osd_hack_score.png"), font, col_osd, "Score:", osd_x, osd_y, function()
		return GAME.hackman.score
	end)

	self.osd_items = { osd_lives, osd_rocks, osd_whak_score, osd_hack_score }

	--[[
	-- Create FPS item
	local col_fps = { r = 0xff, g = 0x7f, b = 0x00, a = 0xff }
	local fps_x,fps_y = SCREEN_WIDTH / 2 - 50, SCREEN_HEIGHT - font_size - 10
	self.fps_item = FPSItem:new(font, col_fps, fps_x, fps_y)
	--]]

	-- Fade surface to provide fade in/out effects
	self.fade_surface = FadeSurface:new()

	-- 2-dimensional array of tiles
	self.tiles = {}
	self:setup_tiles()

	-- Start the game from level 1
	self:load_level(1)
	self.fade_surface:fade_out()

	self.is_end_screen = false

	-- Array for the blinking red cross and arrow for when all the coins are collected
	self.exit_indicators = {}
end

function Game:update(dt)
	-- Update hover phase for collectibles
	Actor.update_hover(dt)

	-- Freeze the characters during fade in/out
	if not self.fade_surface:is_fading() then
		if not self.is_end_screen then
			-- Update Whakman
			self.whakman:update(dt)

			-- Check if Hackman can eat Whakman
			if not self.whakman.dead then
				self:check_lose_condition()
			end

			-- Update Hackman
			self.hackman:update(dt)

			-- Update the blinking indicators
			local index = 1
			while index <= #self.exit_indicators do
				local exit_indicator = self.exit_indicators[index]
				exit_indicator:update(dt)
				if exit_indicator.should_remove then
					table.remove(self.exit_indicators, index)
				else
					index = index + 1
				end
			end
		else
			-- Update only the mouths in the end screen
			if not self.whakman.dead then
				-- Only do Whakman if he survived
				WhakmanCommon.update_mouth(self.whakman, dt)
			end
			WhakmanCommon.update_mouth(self.hackman, dt)
		end
	end

	-- Update FPS if enabled
	if self.fps_item then
		self.fps_item:update(dt)
	end

	-- Update fade in/out
	self.fade_surface:update(dt)
end

function Game:draw()
	-- Don't draw tiles in the end screen
	if not self.is_end_screen then
		-- This will draw the tiles and collectibles
		for y = 1, LEVEL_HEIGHT do
			for x = 1, LEVEL_WIDTH do
				self.tiles[y][x]:draw(self.game_area.x, self.game_area.y)
			end
		end

		-- Draw the blinking indicators
		for _, exit_indicator in ipairs(self.exit_indicators) do
			exit_indicator:draw(self.game_area.x, self.game_area.y)
		end
	end

	-- Draw the characters
	self.whakman:draw(self.game_area.x, self.game_area.y)
	self.hackman:draw(self.game_area.x, self.game_area.y)

	-- Draw the OSD items (Labels for the end screen)
	for _, osd_item in ipairs(self.osd_items) do
		osd_item:draw()
	end

	-- Show FPS if enabled
	if self.fps_item then
		self.fps_item:draw()
	end

	-- Draw the fade surface
	self.fade_surface:draw()
end

-- This will create all the tile objects and assign the corresponding coordinates
function Game:setup_tiles()
	for y = 1, LEVEL_HEIGHT do
		self.tiles[y] = {}

		for x = 1, LEVEL_WIDTH do
			available_directions = {}

			-- Create them as solid walls
			for _, direction in pairs(DIRECTIONS) do
				available_directions[direction] = false
			end

			local tile = Tile:new(available_directions)

			-- Pixel position
			tile.x = (x - 1) * TILE_SIZE
			tile.y = (y - 1) * TILE_SIZE

			-- Tile position
			tile.tile_x = x
			tile.tile_y = y

			self.tiles[y][x] = tile
		end
	end
end

-- Load the level from LEVELS table
-- This will adjust all the tiles, create collectibles and set the starting paths for the characters
function Game:load_level(level)
	if level > #LEVELS or self.whakman.lives == 0 then
		-- Finished all the levels or lost all the lives: show the end screen
		self:show_end_screen()
	else
		self.current_level = level

		-- Reset the coins amount
		self.coins = 0

		local level_map = LEVELS[self.current_level].map

		for y = 1, LEVEL_HEIGHT do
			for x = 1, LEVEL_WIDTH do
				-- Each tile is represented by 3x3 square of characters in LEVELS
				-- level_x and level_y will point to the center of that square
				local level_x, level_y = (x - 1) * 3 + 2, (y - 1) * 3 + 2

				local tile = self.tiles[y][x]
				tile:remove_content()

				tile.available_directions[DIRECTIONS.up]	= level_map[level_y - 1]:sub(level_x, level_x) == TILE_CONTENT.empty
				tile.available_directions[DIRECTIONS.right]	= level_map[level_y]:sub(level_x + 1, level_x + 1) == TILE_CONTENT.empty
				tile.available_directions[DIRECTIONS.down]	= level_map[level_y + 1]:sub(level_x, level_x) == TILE_CONTENT.empty
				tile.available_directions[DIRECTIONS.left]	= level_map[level_y]:sub(level_x - 1, level_x - 1) == TILE_CONTENT.empty

				tile.image = GetTileImage(tile.available_directions)

				local content_type = level_map[level_y]:sub(level_x, level_x)
				if content_type == TILE_CONTENT.coin or content_type == TILE_CONTENT.rock then
					if content_type == TILE_CONTENT.coin then
						-- Increase the total number of coins on the map
						self.coins = self.coins + 1
					end
					-- Create the collectible object in this tile
					tile:add_content(content_type)
				elseif content_type == TILE_CONTENT.whakman then
					-- Specify that Whakman should move to this tile
					self.whakman:enter_level(tile)
				elseif content_type == TILE_CONTENT.hackman then
					-- Specify that Hackman should move to this tile
					self.hackman:enter_level(tile)
				end
			end
		end

		-- Reset the blinking indicators (in case we still had them from previous level)
		self.exit_indicators = {}
	end
end

-- Show the indicators to let the player know that they need to exit on the right
function Game:show_exit_indicators()
	-- Red cross on the left
	local no_left_exit = ExitIndicator:new(Image("red_cross.png"))
	no_left_exit.x, no_left_exit.y = 0, (LEVELS[self.current_level].exit_y - 1) * TILE_SIZE

	-- Red arrow on the right
	local yes_right_exit = ExitIndicator:new(Image("red_arrow.png"))
	yes_right_exit.x, yes_right_exit.y = (LEVEL_WIDTH - 1) * TILE_SIZE, (LEVELS[self.current_level].exit_y - 1) * TILE_SIZE

	self.exit_indicators = {no_left_exit, yes_right_exit}
end

-- Get the tile object and relative x and y in that tile based on x and y in the game area
function Game:get_tile(x, y)
	local tile_x, tile_y = math.floor(x / TILE_SIZE) + 1, math.floor(y / TILE_SIZE) + 1
	local rel_x, rel_y = x - (tile_x - 1) * TILE_SIZE, y - (tile_y - 1) * TILE_SIZE

	if tile_x > 0 and tile_x <= LEVEL_WIDTH
	and tile_y > 0 and tile_y <= LEVEL_HEIGHT then
		return self.tiles[tile_y][tile_x], rel_x, rel_y
	end
end

-- Alias for Game:get_tile
function GetTile(x, y)
	return GAME:get_tile(x, y)
end

-- Check if Hackman is close enough to eat Whakman
function Game:check_lose_condition()
	-- Can't eat during the entering sequence or if Hackman is stunned
	if self.hackman.entering or self.hackman.delay > 0 then
		return
	end

	local whak_x, whak_y, hack_x, hack_y = self.whakman.x, self.whakman.y, self.hackman.x, self.hackman.y
	local whak_tile = GetTile(whak_x, whak_y)
	local hack_tile = GetTile(hack_x, hack_y)

	-- whak_tile may be nil if Whakman exited the level
	if not whak_tile or not hack_tile then
		return
	end

	-- Adjust the coordinates to allow eating through the tunnel
	if whak_tile.tile_x == 1 and hack_tile.tile_x == LEVEL_WIDTH then
		hack_x = hack_x - LEVEL_WIDTH * TILE_SIZE
	elseif hack_tile.tile_x == 1 and whak_tile.tile_x == LEVEL_WIDTH then
		whak_x = whak_x - LEVEL_WIDTH * TILE_SIZE
	end

	local lose_distance = 1.5 * self.whakman.radius
	if math.abs(whak_x - hack_x) <= lose_distance and math.abs(whak_y - hack_y) <= lose_distance then
		-- Whakman is close enough to Hackman: initiate the eating!
		self.whakman.dead = true
		self.whakman:set_state(WhakmanCommon.STATES.stand)
		self.hackman:on_eat_whakman()
	end
end

function Game:show_end_screen()
	self.is_end_screen = true

	-- Show Whakman to the left
	self.whakman.x, self.whakman.y = 2 * TILE_SIZE, self.game_area.h / 2 + 2 * TILE_SIZE
	if not self.whakman.dead then
		-- If Whakman is alive, he should be facing Hackman
		self.whakman.mouth_timer = 0
		self.whakman.image_index = 1
		self.whakman.direction = DIRECTIONS.right
		self.whakman:set_state(WhakmanCommon.STATES.move)
	else
		-- If he's dead, make sure to show the skull
		self.image = Image("skull.png")
	end

	-- And Hackman to the right, facing Whakman
	self.hackman.x, self.hackman.y = self.game_area.w - 2 * TILE_SIZE, self.game_area.h / 2 + 2 * TILE_SIZE
	self.hackman.mouth_timer = 0
	self.hackman.image_index = 2
	self.hackman.direction = DIRECTIONS.left
	self.hackman:set_state(WhakmanCommon.STATES.move)

	-- Hackman may have been stunned, so don't show the birds
	self.hackman.birds = nil

	local font_size = 32
	local font = LoadFont("LondonBetween.ttf", font_size)
	local text_color = { r = 0xff, g = 0xff, b = 0xff, a = 0xff }

	-- Whakman score (below Whakman)
	local text_x, text_y = self.whakman.x + self.game_area.x, self.whakman.y + self.game_area.y + TILE_SIZE
	local whak_score = Label:new(font, text_color, "Score: "..tostring(self.whakman.score), text_x, text_y, true)

	-- Hackman score (below Hackman)
	text_x, text_y = self.hackman.x + self.game_area.x, self.hackman.y + self.game_area.y + TILE_SIZE
	local hack_score = Label:new(font, text_color, "Score: "..tostring(self.hackman.score), text_x, text_y, true)


	-- All the other labels will be horizontally in the center of the window
	text_x = SCREEN_WIDTH / 2

	-- Show THE END in bigger font size
	local font_size_big = 48
	local font_big = LoadFont("LondonBetween.ttf", font_size_big)
	text_y = 100
	local the_end = Label:new(font_big, text_color, "THE END", text_x, text_y, true)

	-- Whether Whakman died or survived
	text_y = 200
	local desc_text
	if self.whakman.dead then
		desc_text = "You died."
	else
		desc_text = "You survived!"
	end
	local desc_label = Label:new(font, text_color, desc_text, text_x, text_y, true)

	-- Comment on player's performance
	text_y = 250
	local flavor_text
	if self.whakman.score > self.hackman.score then
		if self.whakman.dead then
			flavor_text = "Well, at least you got more money."
		else
			flavor_text = "You really showed him!"
		end
	else
		if self.whakman.dead then
			flavor_text = "Better luck next time."
		else
			flavor_text = "Next time try to get more money, too!"
		end
	end
	local flavor_label = Label:new(font, text_color, flavor_text, text_x, text_y, true)

	-- Suggest a course of action
	text_y = 300
	local press_esc = Label:new(font, text_color, "Press ESCAPE to exit", text_x, text_y, true)

	-- Made by me
	text_y = 620
	local made_by = Label:new(font, text_color, "Made by Valentyn Nykoliuk", text_x, text_y, true)

	-- Store these labels instead of OSD items
	self.osd_items = {whak_score, hack_score, the_end, desc_label, flavor_label, press_esc, made_by}
end
