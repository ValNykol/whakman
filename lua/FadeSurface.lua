---------------------------------------------------------------------------------------------------

-- Fade surface

FadeSurface = class()

-- Fading interval, in seconds
FadeSurface.FADE_DURATION = 2

-- Possible fading modes
FadeSurface.FADE_MODE = {
	none = 1,
	fade_in = 2,
	fade_out = 3
}

function FadeSurface:init()
	self.mode = FadeSurface.FADE_MODE.none
	self.fade_timer = 0
	self.alpha = 0xff
end

-- on_complete: a callback to be executed after the fade in is finished
function FadeSurface:fade_in(on_complete)
	self.mode = FadeSurface.FADE_MODE.fade_in
	self.fade_timer = 0
	self:refresh()
	self.on_complete = on_complete
end

-- on_complete: a callback to be executed after the fade out is finished
function FadeSurface:fade_out(on_complete)
	self.mode = FadeSurface.FADE_MODE.fade_out
	self.fade_timer = 0
	self:refresh()
	self.on_complete = on_complete
end

function FadeSurface:update(dt)
	if self.mode ~= FadeSurface.FADE_MODE.none then
		self.fade_timer = self.fade_timer + dt
		-- Redraw the surface
		self:refresh()

		if self.fade_timer >= FadeSurface.FADE_DURATION then
			-- Finished the fading
			self.mode = FadeSurface.FADE_MODE.none

			-- Execute the callback if any
			if self.on_complete then
				self.on_complete()
			end
		end
	end
end

function FadeSurface:is_fading()
	return (self.mode ~= FadeSurface.FADE_MODE.none)
end

-- Fill the surface with black with corresponding alpha
-- Should not be called if the mode is FADE_MODE.none
function FadeSurface:refresh()
	-- Linearly interpolate alpha based on the time passed
	if		self.mode == FadeSurface.FADE_MODE.fade_in then
		alpha = math.floor((self.fade_timer / FadeSurface.FADE_DURATION) * 0xff)
	elseif	self.mode == FadeSurface.FADE_MODE.fade_out then
		alpha = math.floor((1 - self.fade_timer / FadeSurface.FADE_DURATION) * 0xff)
	end

	-- Clamp alpha between 0 and 0xff (255)
	if alpha < 0 then
		alpha = 0
	elseif alpha > 0xff then
		alpha = 0xff
	end
end

function FadeSurface:draw()
	SYS_RENDERER:setDrawColor{ r = 0, g = 0, b = 0, a = alpha }
	SYS_RENDERER:fillRect{ x = 0, y = 0, w = SCREEN_WIDTH, h = SCREEN_HEIGHT }
end
