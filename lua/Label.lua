---------------------------------------------------------------------------------------------------

-- Labels have text to be displayed on the screen, can be centered (horizontally)

Label = class()

function Label:init(font, color, text, x, y, centered)
	self.font = font
	self.color = color
	self.text = text
	self.x = x
	self.y = y
	self.centered = centered
end

function Label:draw()
	if self.centered then
		self.font:print_centered(self.x, self.y, self.color, self.text)
	else
		self.font:print(self.x, self.y, self.color, self.text)
	end
end
